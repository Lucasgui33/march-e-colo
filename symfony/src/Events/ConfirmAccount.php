<?php

namespace App\Events;

use App\Entity\User;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use ApiPlatform\Core\EventListener\EventPriorities;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class ConfirmAccount implements EventSubscriberInterface{

    private $mailer;

    function __construct(Security $security, MailerInterface $mailer){
        $this->security = $security;
        $this->mailer = $mailer;
    }

    // Fonction : Permet de lancer l'événement et les fonctions, après la validation de la requête
    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::VIEW =>['confirmUser', EventPriorities::POST_VALIDATE]
        ];
    }

    // Fonction : Lancée après la validation des infos, permet de cibler la méthode POST pour l'entité User
    public function confirmUser(ViewEvent $event){
        $user = $event->getControllerResult();
        $method = $event->getRequest()->getMethod();

        if($user instanceof User && $method === "POST" ) {
            $this->sendEmailConfirmAccount($user->getEmail(), $user->getFirstName(), $user->getLastName());
        }
    }

    // Fonction : Permet d'envoyer le mail avec le template de confirmation de création d'un compte
    public function sendEmailConfirmAccount($emailUser, $firstnameUser, $lastnameUser)
    {
        // Instanciation de l'objet TemplatedEmail, permettant de récupérer un Twig comme base
        $email = (new TemplatedEmail())
            ->from('marchecolo@contact.com')
            ->to($emailUser)
            ->subject('Création du compte !')
            ->htmlTemplate('mailing/template_confirm_account.html.twig')
            ->context([
                'firstnameUser' => $firstnameUser,
                'lastnameUser' => $lastnameUser,
            ]);

        $this->mailer->send($email);
    }

}
