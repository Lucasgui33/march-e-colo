<?php

namespace App\Events;

use App\Entity\Order;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use ApiPlatform\Core\EventListener\EventPriorities;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class UserOrderInit implements EventSubscriberInterface{

    private $security;
    private $mailer;

    function __construct(Security $security, MailerInterface $mailer){
        $this->security = $security;
        $this->mailer = $mailer;
    }

    // Fonction : Permet de lancer l'événement et les fonctions, après la validation de la requête
    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::VIEW =>['setUserOrder', EventPriorities::POST_VALIDATE]
        ];
    }

    // Fonction : Lancée après la validation des infos, permet de cibler la méthode POST ou DELETE pour l'entité ProductsOrder
    public function setUserOrder(ViewEvent $event){
        $productsOrder = $event->getControllerResult();
        $method = $event->getRequest()->getMethod();

        $user = $this->security->getUser();

        // Condition : Si la méthode est POST alors on confirme une commande en cours
        if($productsOrder instanceof Order && $method === "POST" ) {
            $productsOrder->setUser($user);

            $this->sendEmailValidationCommand($user->getEmail(), $productsOrder->getTotalPrice(), $productsOrder->getProductsOrders());
        }

        // Condition : Si la méthode est DELETE alors on tente de supprimer la commande en cours
        if($productsOrder instanceof Order && $method === "DELETE" ) {
            $this->sendEmailSuppressionCommand($productsOrder->getUser()->getEmail());
        }
    }

    // Fonction : Permet d'envoyer le mail avec le template de confirmation d'une commande
    public function sendEmailValidationCommand($emailUser, $totalPrice, $products)
    {
        $email = (new TemplatedEmail())
            ->from('marchecolo@contact.com')
            ->to($emailUser)
            ->subject('Validation de la commande')
            ->htmlTemplate('mailing/template_validation_command.html.twig')
            ->context([
                'totalPrice' => $totalPrice,
                'products' => $products
            ]);

        $this->mailer->send($email);
    }

    // Fonction : Permet d'envoyer le mail avec le template de suppression d'une commande
    public function sendEmailSuppressionCommand($emailUser)
    {
        $email = (new TemplatedEmail())
            ->from('marchecolo@contact.com')
            ->to($emailUser)
            ->subject('Suppression de la commande')
            ->htmlTemplate('mailing/template_suppression_command.html.twig');

        $this->mailer->send($email);
    }

}
