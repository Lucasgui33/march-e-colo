<?php

namespace App\Events;


use App\Entity\User;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use ApiPlatform\Core\EventListener\EventPriorities;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Security\Core\Security;

class UserSubscriber implements EventSubscriberInterface{

    private $security;

    function __construct(Security $security){
        $this->security = $security;
    }

    // Fonction : Permet de lancer l'événement et les fonctions, avant la validation de la requête
    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::VIEW =>['setUserProduct', EventPriorities::PRE_VALIDATE]
        ];
    }

    // Fonction : Lancée avant la validation des infos, permet de cibler la méthode POST User
    public function setUserProduct(ViewEvent $event){
        $user = $event->getControllerResult();

        $method = $event->getRequest()->getMethod();
        if($user instanceof User && $method === "POST" )   {
        // $user->setUser($user);
        }
    }

}
