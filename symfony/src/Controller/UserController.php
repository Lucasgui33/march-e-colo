<?php

namespace App\Controller;

use Exception;
use App\Repository\ProductsRepository;
use App\Repository\UserRepository;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class UserController extends AbstractController
{
     
        private $security;
    
        function __construct(Security $security){
            $this->security = $security;
        }
    
        /**
         * @Route("/api/myprofile", name="myprofile")
         * Renvoie l'utilisateur courant
         */
        public function myprofile(UserRepository $RepoUser,SerializerInterface $serialiezer): Response
        {
            
            
            try{
                $user = $this->security->getUser();
                $id = $user->getId();
                $myself = $RepoUser->find($id);
                $finalRequest = $serialiezer->serialize($myself, 'json');
                // On instancie la réponse
                $response = new Response($finalRequest);
    
                // On ajoute l'entête HTTP
                $response->headers->set('Content-Type', 'application/json');
    
                // On envoie la réponse
                return $response;
            }catch(Exception $e){
                return new Response('Failed', 404);
            }
    
        }

        /**
         * @Route("/api/myproducts", name="myproducts")
         * Renvoie les produits de l'utilisateur connecté
         */
        public function myproducts(ProductsRepository $RepoProducts,SerializerInterface $serialiezer): Response
        {
            try{
                $user = $this->security->getUser();
                $id=$user->getId();
                $myProducts = $RepoProducts->findBy(array('user' => $id));
                $finalRequest = $serialiezer->serialize($myProducts, 'json');
                // On instancie la réponse
                $response = new Response($finalRequest);
    
                // On ajoute l'entête HTTP
                $response->headers->set('Content-Type', 'application/json');
    
                // On envoie la réponse
                return $response;
            }catch(Exception $e){
                return new Response('Failed', 404);
            }
        }

        /**
         * @Route("/api/myroles", name="myroles")
         * Renvoie le role du user connecté
         */
        public function myroles(ProductsRepository $RepoProducts,SerializerInterface $serialiezer): Response
        {
            try{
                $user = $this->security->getUser();
                $roles = $user->getRoles();

                //dd($myProducts);
                $finalRequest = $serialiezer->serialize($roles, 'json');
                // On instancie la réponse
                $response = new Response($finalRequest);
    
                // On ajoute l'entête HTTP
                $response->headers->set('Content-Type', 'application/json');
    
                // On envoie la réponse
                return $response;
            }catch(Exception $e){
                return new Response('Failed', 404);
            }
    
        }

        /**
         * @Route("/api/getrequests", name="getrequests")
         * Renvoie les utilisateurs qui veulent passer agriculteur
         */
        public function getrequests(userRepository $RepoUser,SerializerInterface $serialiezer): Response
        {
            
            try{
                $user = $this->security->getUser();

                if(!in_array("ROLE_ADMIN", $user->getRoles()) ){
                    return new Response('Permission denied', 403);
                }

                $myUsers = $RepoUser->findBy(array('askProducer' => true));
                
                $finalRequest = $serialiezer->serialize($myUsers, 'json');
                // On instancie la réponse
                $response = new Response($finalRequest);
    
                // On ajoute l'entête HTTP
                $response->headers->set('Content-Type', 'application/json');
    
                // On envoie la réponse
                return $response;
            }catch(Exception $e){
                return new Response('Failed', 404);
            }
    
        }

        
}
