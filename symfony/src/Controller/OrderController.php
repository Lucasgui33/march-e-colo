<?php

namespace App\Controller;

use Exception;
use App\Entity\User;
use App\Repository\OrderRepository;
use App\Repository\ProductsRepository;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class OrderController extends AbstractController
{
    private $security;

    function __construct(Security $security){
        $this->security = $security;
    }

    /**
     * @Route("/api/myorders", name="myorders")
     * Acceder au commandes de l'utilisateur connecté
     */
    public function myOrder(OrderRepository $RepoOrder,SerializerInterface $serialiezer): Response
    {
        
        
        try{
            $user = $this->security->getUser();
            $myorders = $RepoOrder->findByUser($user);
            // dd($myorders);
            $finalRequest = $serialiezer->serialize($myorders, 'json', ['groups' => 'order:read']);
            
            // On instancie la réponse
            $response = new Response($finalRequest);

            // On ajoute l'entête HTTP
            $response->headers->set('Content-Type', 'application/json');

            // On envoie la réponse
            return $response;
        }catch(Exception $e){
            return new Response('Failed', 404);
        }

        


    }
    /**
     * @Route("/api/myproducerorders", name="myproducerorders")
     * Acceder au commandes qui concerne l'agriculteur courant connecté
     */
    public function myproducerorders(OrderRepository $RepoProducts,SerializerInterface $serialiezer): Response
    {
        
        
        try{
            $user = $this->security->getUser();
            $id = $user->getId();
            //$myProducts = $RepoProducts->findBy(array('user' => $id));
            $myProducts = $RepoProducts->findAll();

            $finalRequest = $serialiezer->serialize($myProducts, 'json');
            //$finalRequest = $serialiezer->serialize($myProducts, 'json', ['groups' => 'group1'] );
            // On instancie la réponse
            $response = new Response($finalRequest);

            // On ajoute l'entête HTTP
            $response->headers->set('Content-Type', 'application/json');

            // On envoie la réponse
            return $response;
        }catch(Exception $e){
            return new Response('Failed', 404);
        }

    }
}
