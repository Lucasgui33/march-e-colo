<?php

namespace App\Controller;

use Exception;
use App\Service\FileUploader;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ProductsImagesController extends AbstractController
{
    /**
     * @Route("api/picture/{path}", name="picture", methods={"GET"})
     * 
     */
    public function getPicture(Request $request,FileUploader $fileUploader): Response
    {
        try{
            //récupération de l'image
            $file = file_get_contents($fileUploader->getTargetDirectory().'/'.$request->get('path'));
            
            // On instancie la réponse avec l'image
            return new Response($file);
            
        }catch(Exception $e){
            return new Response($e, 404);
        }
        
    }

}
