<?php

namespace App\Controller;

use App\Entity\Products;
use App\Entity\Category;
use App\Service\FileUploader;
use App\Entity\ProductsImages;
use Symfony\Component\Security\Core\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

class ImageController extends AbstractController
{
    private $security;
    private $fileUploader;
    
    function __construct(Security $security, FileUploader $fileUploader){
        $this->security = $security;
        $this->fileUploader = $fileUploader;
    }

    public function __invoke(Products $product, Request $request,SerializerInterface $serialiezer): Response {
        $entityManager = $this->getDoctrine()->getManager();
        $user = $this->security->getUser();

        $product->setUser($user);
        $product->setName($request->get('name'));
        $product->setDescription($request->get('description'));
        $product->setUnitPrice($request->get('unit_price'));
        $product->setunity($request->get('unity'));
        $product->setStock($request->get('stock'));
        $product->setVariety($request->get('variety'));
        //récupération de la catégorie
        $category = $this->getDoctrine()->getRepository(Category::class)->find($request->get('category'));
        $product->setCategory($category);
        $entityManager->persist($product);
        $entityManager->flush();

        $pictures = $request->files->get('images');
        
        for($i = 0; $i < count($pictures); $i++){
            $image = $pictures[$i];
            // $image = $pictures;
            
            $fileName = $this->fileUploader->upload($image);//sauvegarde de l'image grâce au service FileUploader
            $picture = new ProductsImages();
            $picture->setImagePath('images/' . $fileName);
            $picture->setProduct($product);// on hydrate l'image avec le produit
            $entityManager->persist($picture);
        }

        $entityManager->flush();
        // On instancie la réponse
        $finalRequest = $serialiezer->serialize($product, 'json');
        $response = new Response($finalRequest);

        // On ajoute l'entête HTTP
        $response->headers->set('Content-Type', 'application/json');

        // On envoie la réponse
        return $response;
    }
}
