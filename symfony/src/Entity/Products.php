<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\ProductsRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\Serializer\Annotation\Groups;
use Doctrine\ORM\Mapping as ORM;
use App\Controller\ImageController;

/**
 * @ApiResource(formats={"json"},normalizationContext={"groups"={"user:read"}}, denormalizationContext={"groups"={"product:write"}},
 * collectionOperations={
 *  "get",
 *  "post"={
 *      "path"="/producte",
 *      "method"="POST",
 *      "controller"=ImageController::class,
 *      "deserialize"=false,
 *      "input_formats"={
 *          "multipart"={"multipart/form-data"}
 *      }
 *   }
 * })
 * @ORM\Entity(repositoryClass=ProductsRepository::class)
 */
class Products
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"user:read", "order:read"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     * @Groups({"user:read", "order:read", "product:write"})
     */
    private $name;

    /**
     * @ORM\Column(type="text")
     * @Groups({"user:read", "order:read", "product:write"})
     */
    private $description;

    /**
     * @ORM\ManyToOne(targetEntity=Category::class, inversedBy="products")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"user:read", "order:read", "product:write"})
     */
    private $category;

    /**
     * @ORM\Column(type="decimal", precision=5, scale=2)
     * @Groups({"user:read", "order:read", "product:write"})
     */
    private $unit_price;

    /**
     * @ORM\Column(type="string", length=20)
     * @Groups({"user:read", "order:read", "product:write"})
     */
    private $unity;

    /**
     * @ORM\Column(type="integer")
     * @Groups({"user:read", "product:write"})
     */
    private $stock;

    /**
     * @ORM\OneToMany(targetEntity=ProductsOrder::class, mappedBy="product", orphanRemoval=true)
     * @Groups({"user:read"})
     */
    private $productsOrders;

    /**
     * @ORM\OneToMany(targetEntity=ProductsImages::class, mappedBy="product", orphanRemoval=true)
     * @Groups({"user:read", "order:read"})
     */
    private $productsImages;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="products")
     * @Groups({"user:read", "order:read"})
     */
    private $user;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"user:read", "product:write"})
     */
    private $variety;

    public function __construct()
    {
        $this->productsOrders = new ArrayCollection();
        $this->productsImages = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getCategory(): ?Category
    {
        return $this->category;
    }

    public function setCategory(?Category $category): self
    {
        $this->category = $category;

        return $this;
    }

    public function getUnitPrice(): ?string
    {
        return $this->unit_price;
    }

    public function setUnitPrice(string $unit_price): self
    {
        $this->unit_price = $unit_price;

        return $this;
    }

    public function getUnity(): ?string
    {
        return $this->unity;
    }

    public function setUnity(string $unity): self
    {
        $this->unity = $unity;

        return $this;
    }

    public function getStock(): ?int
    {
        return $this->stock;
    }

    public function setStock(int $stock): self
    {
        $this->stock = $stock;

        return $this;
    }

    /**
     * @return Collection|ProductsOrder[]
     */
    public function getProductsOrders(): Collection
    {
        return $this->productsOrders;
    }

    public function addProductsOrder(ProductsOrder $productsOrder): self
    {
        if (!$this->productsOrders->contains($productsOrder)) {
            $this->productsOrders[] = $productsOrder;
            $productsOrder->setProduct($this);
        }

        return $this;
    }

    public function removeProductsOrder(ProductsOrder $productsOrder): self
    {
        if ($this->productsOrders->removeElement($productsOrder)) {
            // set the owning side to null (unless already changed)
            if ($productsOrder->getProduct() === $this) {
                $productsOrder->setProduct(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|ProductsImages[]
     */
    public function getProductsImages(): Collection
    {
        return $this->productsImages;
    }

    public function addProductsImage(ProductsImages $productsImage): self
    {
        if (!$this->productsImages->contains($productsImage)) {
            $this->productsImages[] = $productsImage;
            $productsImage->setProduct($this);
        }

        return $this;
    }

    public function removeProductsImage(ProductsImages $productsImage): self
    {
        if ($this->productsImages->removeElement($productsImage)) {
            // set the owning side to null (unless already changed)
            if ($productsImage->getProduct() === $this) {
                $productsImage->setProduct(null);
            }
        }

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getVariety(): ?string
    {
        return $this->variety;
    }

    public function setVariety(string $variety): self
    {
        $this->variety = $variety;

        return $this;
    }
}
