<?php

namespace App\DataFixtures;

use App\Entity\Category;
use App\Entity\Order;
use App\Entity\Products;
use App\Entity\ProductsImages;
use App\Entity\ProductsOrder;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class AppFixtures extends Fixture
{
    private $encoder;

    public function __construct(UserPasswordHasherInterface $encoder)
    {
        $this->encoder = $encoder;
    }
    public function load(ObjectManager $manager): void
    {
        $faker = Factory::create('fr_FR');
        $initialCategories = ['fruits', 'légumes'];
        $orderStatus = ['active', 'pending', 'delivered'];
        $array_category = [];
        $array_products = [];

        // Boucle : Crée 5 catégories
        for ($ca = 0; $ca < 2; $ca++) {
            $category = new Category();
            $category->setName($initialCategories[$ca]);
            $manager->persist($category);
            $array_category[] = $category;
        }

        // Boucle : Crée 10 utilisateurs
        for ($u = 0; $u < 15; $u++) {
            // Ajout utilisateurs de test : Client, Producteur, Admin
            $user= new User();
            if($u == 0) {
                $user->setEmail('test@client.com');
                $user->setFirstName($faker->firstName);
                $user->setLastName($faker->lastName);
                $user->setRoles(["ROLE_CLIENT"]);
                $user->setTelephone("");
                $user->setPresentation("");
                $user->setPassword($this->encoder->hashPassword($user, "client"));
                $user->setProducerName("Abricots Coco");
                $manager->persist($user);
            } else if($u == 1) {
                $user->setEmail('test@producer.com');
                $user->setFirstName($faker->firstName);
                $user->setLastName($faker->lastName);
                $user->setRoles(["ROLE_FARMER"]);
                $user->setTelephone("0586547698");
                $user->setPresentation("Je me présente, je m'appelle Henri et j'aimerai bien réussir ma vie");
                $user->setPassword($this->encoder->hashPassword($user, "producer"));
                $user->setProducerName("Le verger des dunes");
                $manager->persist($user);

            } else if($u == 2) {
                $user->setEmail('test@admin.com');
                $user->setFirstName($faker->firstName);
                $user->setLastName($faker->lastName);
                $user->setRoles(["ROLE_ADMIN"]);
                $user->setTelephone("");
                $user->setPresentation("");
                $user->setPassword($this->encoder->hashPassword($user, "admin"));
                $user->setProducerName("");
                $manager->persist($user);
            } else {
                $user->setEmail($faker->email);
                $user->setFirstName($faker->firstName);
                $user->setLastName($faker->lastName);
                $user->setTelephone("");
                $user->setPresentation("");
                $user->setPassword($this->encoder->hashPassword($user, "user1"));
                $user->setProducerName("Les Pommes d'Api");
                $manager->persist($user);
            }

            // /images/
            // Boucle : Crée 5 produits
            if(!in_array("ROLE_ADMIN",$user->getRoles())) {
                for ($p = 0; $p < 5; $p++) {
                    $product = new Products();
                    $product->setName($faker->text(10));
                    $product->setDescription($faker->text(200));
                    $product->setCategory($array_category[rand(0,1)]);
                    $product->setUser($user);
                    $product->setUnitPrice(rand(1,15));
                    if( 1==rand(1,2)){
                        $product->setUnity('pièce(s)');
                    }
                    else{
                        $product->setUnity('kg');  
                    }  
                    $product->setStock(rand(200, 6000));
                    $product->setVariety("Amandine");
                    $manager->persist($product);
    
                    $array_products[] = $product;
    
                    // Boucle : Crée des images de produit
                    for ($pi = 0; $pi < rand(1,2); $pi++) {
                        $product_images = new ProductsImages();
                        $imagePath="images/".rand(1,3).".jpg";
                        $product_images->setImagePath($imagePath);
                        $product_images->setProduct($product);
                        $manager->persist($product_images);
                    }
                }
            }

            if(in_array("ROLE_CLIENT",$user->getRoles())) {
                // Boucle : Pour chaque client crée, faire 9 commandes chacun
                for ($o = 0; $o < 9; $o++) {
                    $order = new Order();
                    $order->setUser($user)
                        ->setStatus($orderStatus[rand(0,2)])
                        ->setTotalPrice($faker->randomDigitNotNull);
                    $manager->persist($order);

                    // Boucle : Crée des commandes pour le produit en cours
                    for ($po = 0; $po < rand(2,5); $po++) {
                        $product_order = new ProductsOrder();
                        $product_order->setProduct($array_products[rand(0,4)]);
                        $product_order->setPrice(rand(1,20));
                        $product_order->setOrder($order);
                        $product_order->setQuantity(rand(1,10));
                        $manager->persist($product_order);
                    }
                }
            }
            
        }

        $manager->flush();

    }
}
