# Symfony+ API plateform (Back)

## Prérequis

<a href="https://www.php.net/downloads" target="_blank"> - PHP</a>

<a href="https://symfony.com/download" target="_blank"> - Symfony</a>

<a href="https://github.com/mailhog/MailHog/releases/tag/v1.0.1" target="_blank"> - Mailhog</a>


Soyez sur d'avoir une base de données accessible et si vous n'en avez pas, vous pouvez installer WAMP en cliquant <a href="https://www.wampserver.com/en/download-wampserver-64bits/" target="_blank">ici</a>


## Installation et lancement

configurez votre fichier .env et décommentez-y "MAILER_DSN=..."

```
composer install
php bin/console doctrine:database:create --if-not-exists 
php bin/console doctrine:schema:update --force 
php bin/console doctrine:fixtures:load -n
symfony server:start
```

<a href="http://127.0.0.1:8000/api/" target="_blank">Accédez à votre API</a>

## Problème potentiel

Si vous rencontrez l'erreur "could not find driver" à l'exécution de la commande doctrine n'oubliez pas d'init "PDO" dans votre php.ini
