<?php

namespace App\Tests\Unit;

use App\Entity\User;
use PHPUnit\Framework\TestCase;

class UserTest extends TestCase {

    private $user;

    


    public function setUp(): void {
        parent::setUp();
        $this->user = new User();
    }

    // public function testAddDate() {
    //     $date = new \DateTime("now");

    //     $result = $this->booking->setDate($date);

    //     self::assertInstanceOf(Booking::class, $result);
    //     self::assertEquals($date, $this->booking->getDate());
    // }


    public function testAddEmail() {
        $mail = "test@projet.com";

        $result = $this->user->setEmail($mail);

        self::assertInstanceOf(User::class, $result);
        self::assertEquals($mail, $this->user->getEmail());
    }

    public function testAddRole() {
        $role = "ROLE_FARMER";

        $result = $this->user->setRoles(["ROLE_FARMER"]);
        self::assertInstanceOf(User::class, $result);
        self::assertTrue(in_array($role, $this->user->getRoles()));
    }

    public function testAddFirstName() {
        $firstName = "Yoann";

        $result = $this->user->setFirstName($firstName);
        
        self::assertInstanceOf(User::class, $result);
        self::assertEquals($firstName, $this->user->getFirstName());
    }

    public function testAddLastName() {
        $lastName = "SINGER";

        $result = $this->user->setLastName($lastName);

        self::assertInstanceOf(User::class, $result);
        self::assertEquals($lastName, $this->user->getLastName());
    }

    public function testAddPresentation() {
        $presentation = "Je me présente je m'appelle Henri";

        $result = $this->user->setPresentation($presentation);

        self::assertInstanceOf(User::class, $result);
        self::assertEquals($presentation, $this->user->getPresentation());
    }

    public function testAddAdresse() {
        $adresse = "42 rue de l'internet 33000 Bordeaux";

        $result = $this->user->setAdresse($adresse);

        self::assertInstanceOf(User::class, $result);
        self::assertEquals($adresse, $this->user->getAdresse());
    }

    public function testAddProducerName() {
        $producerName = "EURL la bonne patate";

        $result = $this->user->setProducerName($producerName);

        self::assertInstanceOf(User::class, $result);
        self::assertEquals($producerName, $this->user->getProducerName());
    }

    public function testAddAskProducer() {
        $ask = true;

        $result = $this->user->setAskProducer($ask);

        self::assertInstanceOf(User::class, $result);
        self::assertEquals($ask, $this->user->getAskProducer());
  
    }

    public function testAddTelephone() {
        $tel = "0666666666";

        $result = $this->user->setTelephone($tel);

        self::assertInstanceOf(User::class, $result);
        self::assertEquals($tel, $this->user->getTelephone());
    }

    // public function testBookedBy() {
    //     $user = new User();

    //     $result = $this->booking->setBookedBy($user);

    //     self::assertInstanceOf(Booking::class, $result);
    //     self::assertEquals($user, $this->booking->getBookedBy());
    // }

}