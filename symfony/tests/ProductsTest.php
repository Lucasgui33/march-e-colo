<?php

namespace App\Tests\Unit;

use App\Entity\User;
use App\Entity\Booking;
use App\Entity\Category;
use App\Entity\Products;
use PHPUnit\Framework\TestCase;

class ProductsTest extends TestCase {

    private $product;

    public function setUp(): void {
        parent::setUp();
        $this->product = new Products();
    }

    public function testSetName() {
        $name = "Product test";

        $result = $this->product->setName($name);

        self::assertInstanceOf(Products::class, $result);
        self::assertEquals($name, $this->product->getName());
    }

    public function testSetDescription() {
        $desc = "Je suis une description longue trés longue";

        $result = $this->product->setDescription($desc);

        self::assertInstanceOf(Products::class, $result);
        self::assertEquals($desc, $this->product->getDescription());
    }

    public function testAddCategory() {
        $category = new Category();

        $result = $this->product->setCategory($category);

        self::assertInstanceOf(Products::class, $result);
        self::assertEquals($category, $this->product->getCategory());
    }

    public function testSetUnitPrice() {
        $price = 10;

        $result = $this->product->setUnitPrice($price);

        self::assertInstanceOf(Products::class, $result);
        self::assertEquals($price, $this->product->getUnitPrice());
    }

    public function testSetUnity() {
        $unit = "pièces";

        $result = $this->product->setUnity($unit);

        self::assertInstanceOf(Products::class, $result);
        self::assertEquals($unit, $this->product->getUnity());
    }

    public function testSetStock() {
        $stock = 42;

        $result = $this->product->setStock($stock);

        self::assertInstanceOf(Products::class, $result);
        self::assertEquals($stock, $this->product->getStock());
    }

    public function testAddVariety() {
        $variety = "Variety";

        $result = $this->product->setVariety($variety);

        self::assertInstanceOf(Products::class, $result);
        self::assertEquals($variety, $this->product->getVariety());
    }

    public function testAddUser() {
        $user = new User();

        $result = $this->product->setUser($user);

        self::assertInstanceOf(Products::class, $result);
        self::assertEquals($user, $this->product->getUser());
    }

    // public function testAddDate() {
    //     $date = new \DateTime("now");

    //     $result = $this->booking->setDate($date);

    //     self::assertInstanceOf(Booking::class, $result);
    //     self::assertEquals($date, $this->booking->getDate());
    // }


    // public function testStatus() {
    //     $status = "pending";

    //     $result = $this->booking->setStatus($status);

    //     self::assertInstanceOf(Booking::class, $result);
    //     self::assertEquals($status, $this->booking->getStatus());
    // }

    // public function testBookedBy() {
    //     $user = new User();

    //     $result = $this->booking->setBookedBy($user);

    //     self::assertInstanceOf(Booking::class, $result);
    //     self::assertEquals($user, $this->booking->getBookedBy());
    // }

}