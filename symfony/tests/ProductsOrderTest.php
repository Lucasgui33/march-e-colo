<?php

namespace App\Tests\Unit;

use App\Entity\Order;
use App\Entity\Products;
use App\Entity\ProductsOrder;
use PHPUnit\Framework\TestCase;

class ProductsOrderTest extends TestCase {

    private $productsOrder;

    


    public function setUp(): void {
        parent::setUp();
        $this->productsOrder = new ProductsOrder();
    }

    public function testAddQuantity() {
        $qty = 10;

        $result = $this->productsOrder->setQuantity($qty);

        self::assertInstanceOf(ProductsOrder::class, $result);
        self::assertEquals($qty, $this->productsOrder->getQuantity());
    }


    public function testAddProduct() {
        $product = new Products();

        $result = $this->productsOrder->setProduct($product);

        self::assertInstanceOf(ProductsOrder::class, $result);
        self::assertEquals($product, $this->productsOrder->getProduct());
    }

    public function testAddOrder() {
        $order = new Order();

        $result = $this->productsOrder->setOrder($order);

        self::assertInstanceOf(ProductsOrder::class, $result);
        self::assertEquals($order, $this->productsOrder->getOrder());
    }

}