<?php

namespace App\Tests\Unit;

use App\Entity\Products;
use App\Entity\ProductsImages;
use PHPUnit\Framework\TestCase;

class ProductsImagesTest extends TestCase {

    private $productsImages;

    


    public function setUp(): void {
        parent::setUp();
        $this->productsImages = new ProductsImages();
    }

    public function testAddImagePath() {
        $path = "/public/img.png";

        $result = $this->productsImages->setImagePath($path);

        self::assertInstanceOf(ProductsImages::class, $result);
        self::assertEquals($path, $this->productsImages->getImagePath());
    }

    public function testAddProduct() {
        
        $product = new Products();

        $result = $this->productsImages->setProduct($product);

        self::assertInstanceOf(ProductsImages::class, $result);
        self::assertEquals($product, $this->productsImages->getProduct());
    }

}