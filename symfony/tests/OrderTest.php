<?php

namespace App\Tests\Unit;

use App\Entity\Order;
use PHPUnit\Framework\TestCase;

class OrderTest extends TestCase {

    private $order;

    


    public function setUp(): void {
        parent::setUp();
        $this->order = new Order();
    }

    public function testDate() {
        $date = new \DateTimeImmutable();

        $result = $this->order->setCreatedAt($date);

        self::assertInstanceOf(Order::class, $result);
        self::assertEquals($date, $this->order->getCreatedAt());
    }


    public function testStatus() {
        $status = "En attente";

        $result = $this->order->setStatus($status);

        self::assertInstanceOf(Order::class, $result);
        self::assertEquals($status, $this->order->getStatus());
    }


}