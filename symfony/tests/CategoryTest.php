<?php

namespace App\Tests\Unit;

use App\Entity\Category;
use PHPUnit\Framework\TestCase;

class CategoryTest extends TestCase {

    private $category;

    
    public function setUp(): void {
        parent::setUp();
        $this->category = new Category();
    }

    public function testSetName() {
        $name = "Category test";

        $result = $this->category->setName($name);

        self::assertInstanceOf(Category::class, $result);
        self::assertEquals($name, $this->category->getName());
    }

}