import React, {useState, useEffect, useContext} from "react";
import ProductCard from "../../../components/cards/ProductCard";
import usePagination from "../../../hooks/usePagination";
import Pagination from "../../../components/Pagination";
import {ProductsContext} from "../../../contexts/ProductsContext";
import {Link} from "react-router-dom";

export default function MyProducts() {
  const [products, setProducts] = useState([]);
  const [isloading, setIsLoading] = useState(false);
  const [reload, setReload] = useState(false);

  const {getProducerProducts} = useContext(ProductsContext);
  const {deleteProduct} = useContext(ProductsContext);

  const fetchData = async () => {
    const p = await getProducerProducts();
    setProducts(p);
    setIsLoading(false);
  };

  useEffect(() => {
    setIsLoading(true);
    fetchData();
    setReload(false);
  }, [reload]);

  const onDelete = async (id) => {
    await deleteProduct(id);
    setReload(true);
  }

  const {
    next,
    prev,
    jump,
    currentPage,
    maxPage,
    startIndex,
    endIndex,
    paginate,
    jumpIndex
  } = usePagination(products, 8);

  return (
    <>
      <div className="flex  justify-center grid grid-cols-1 divide-y-4 divide-green-400">
        <div className="p-2 flex justify-between lg:mx-20 md:mx-10 sm:mx-10 xs:mx-5">
          <h1 className="text-3xl font-bold">Mes produits</h1>
          <div className="flex items-center justify-center">
            <button className="bg-indigo-600 text-white rounded-xl shadow-md p-2">
              <Link to="/producer/products/new">Ajouter un produit</Link>
            </button>
          </div>
        </div>
        {!isloading ? (
          <>
            <div className="grid lg:grid-cols-4 md:grid-cols-3 sm:grid-cols-2 xs:grid-cols-1 gap-10 items-center h-full mx-6 py-4">
              {products.slice(startIndex, endIndex).map((product) => {
                return (
                  <ProductCard
                    key={product.id}
                    product={product}
                    role="producer"
                    onDelete={(id) => onDelete(id)}
                  />
                );
              })}
            </div>
            <Pagination
              currentPage={currentPage}
              next={next}
              prev={prev}
              jump={jump}
              paginate={paginate}
              maxPage={maxPage}
              jumpIndex={jumpIndex}
            />
          </>
        ) : (
          <div className="flex justify-center py-4">Chargement ...</div>
        )}
      </div>
    </>
  );
}
