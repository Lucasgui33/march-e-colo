import React, {useState, useEffect, useContext} from "react";
import {useHistory} from "react-router-dom";
import {CategoryContext} from "../../../contexts/CategoryContext";
import {ProductsContext} from "../../../contexts/ProductsContext";

export default function AddProduct() {
  const history = useHistory();
  const [name, setName] = useState("");
  const [description, setDescription] = useState("");
  const [variety, setVariety] = useState("");
  const [category, setCategory] = useState("test");
  const [unitPrice, setUnitPrice] = useState(0.0);
  const [unity, setUnity] = useState("kg");
  const [stock, setStock] = useState(0);
  const [productsImages, setProductsImages] = useState([]);

  const {getCategories} = useContext(CategoryContext);
  const {addProduct} = useContext(ProductsContext);
  const [categories, setCategories] = useState([]);
  const [isLoading, setIsLoading] = useState(false);

  const fetchData = async () => {
    const c = await getCategories();
    setCategories(c);
    setCategory(c[0])
    setIsLoading(false);
  }

  useEffect(() => {
    setIsLoading(true)
    fetchData();
  }, []);

  const submitProduct = async () => {
    if (formValid()) {
      const res = await addProduct(name, description, variety, category, unitPrice, unity, stock, productsImages);
      if(typeof res.id === "number") {
        history.push('/producer/products');
      }
    }
  };

  const formValid = () => {
    return (
      name !== "" &&
      variety !== "" &&
      category !== "" &&
      unitPrice !== 0.0 &&
      unity !== "" &&
      stock > 0
    );
  };

  return (
    <>
      <div className="flex justify-center grid grid-cols-1 divide-y-4 divide-green-400 lg:mx-40 md:mx-20 sm:mx-20 xs:mx-10">
        <div className="p-2 flex justify-between">
          <h1 className="text-3xl font-bold">Ajouter un produit</h1>
        </div>
        <div className="flex w-full items-center px-4 py-4">
          
        {isLoading ? (
          <div className="flex items-center">Chargement ...</div>
        ): (
          <div className="relative flex flex-col min-w-0 break-words w-full mb-6 shadow-lg rounded-lg bg-gray-400 border-0">
            <div className="rounded-t mb-0 px-6 py-6">
              <form>
                <div className="relative w-full mb-3">
                  <label
                    className="block uppercase text-gray-700 text-xs font-bold mb-2"
                    htmlFor="name"
                  >
                    Nom du produit <span className="text-red-500">*</span>
                  </label>
                  <input
                    id="name"
                    type="text"
                    required
                    className="border-0 px-3 py-3 placeholder-gray-500 text-gray-700 bg-white rounded text-sm shadow focus:outline-none focus:ring w-full"
                    placeholder="Ex : Patate douce ..."
                    style={{transition: "all .15s ease"}}
                    onBlur={(event) => setName(event.target.value)}
                  />
                </div>

                <div className="relative w-full mb-3">
                  <label
                    className="block uppercase text-gray-700 text-xs font-bold mb-2"
                    htmlFor="productsImages"
                  >
                    Image(s) du produit
                  </label>
                  <input
                    id="productsImages"
                    type="file"
                    multiple={true}
                    className="border-0 px-3 py-3 placeholder-gray-500 text-gray-700 bg-white rounded text-sm shadow focus:outline-none focus:ring w-full"
                    style={{transition: "all .15s ease"}}
                    onChange={(event) => setProductsImages([...productsImages, event.target.files])}
                  />
                </div>

                <div className="relative w-full mb-3">
                  <label
                    className="block uppercase text-gray-700 text-xs font-bold mb-2"
                    htmlFor="description"
                  >
                    Description
                  </label>
                  <textarea
                    id="description"
                    rows={5}
                    className="border-0 px-3 py-3 placeholder-gray-500 text-gray-700 bg-white rounded text-sm shadow focus:outline-none focus:ring w-full"
                    placeholder="Ex : Pomme de terre exotique cultivée dans les plaines de la Creuse ....."
                    style={{transition: "all .15s ease"}}
                    onBlur={(event) => setDescription(event.target.value)}
                  ></textarea>
                </div>

                <div className="grid grid-cols-2 gap-6">
                  <div className="relative w-full mb-3">
                    <label
                      className="block uppercase text-gray-700 text-xs font-bold mb-2"
                      htmlFor="category"
                    >
                      Catégorie <span className="text-red-500">*</span>
                    </label>
                    <select
                      id="category"
                      required
                      className="border-0 px-3 py-3 placeholder-gray-500 text-gray-700 bg-white rounded text-sm shadow focus:outline-none focus:ring w-full"
                      onChange={(event) => setCategory(event.target.value)}
                    >
                      { categories.map((c) => {
                        return (<option key={c.id} value={c.id}>{c.name}</option>)
                      })}
                    </select>
                  </div>
                  <div className="relative w-full mb-3">
                    <label
                      className="block uppercase text-gray-700 text-xs font-bold mb-2"
                      htmlFor="variety"
                    >
                      Variété <span className="text-red-500">*</span>
                    </label>
                    <input
                      id="variety"
                      type="text"
                      required
                      className="border-0 px-3 py-3 placeholder-gray-500 text-gray-700 bg-white rounded text-sm shadow focus:outline-none focus:ring w-full"
                      placeholder="Ex : Granny Smith ..."
                      style={{transition: "all .15s ease"}}
                      onBlur={(event) => setVariety(event.target.value)}
                    />
                  </div>
                </div>

                <div className="grid grid-cols-3 gap-4">
                  <div className="relative w-full mb-3">
                    <label
                      className="block uppercase text-gray-700 text-xs font-bold mb-2"
                      htmlFor="unitPrice"
                    >
                      Prix unitaire ( en € )&nbsp;<span className="text-red-500">*</span>
                    </label>
                    <input
                      id="unitPrice"
                      type="number"
                      required
                      placeholder="Ex : 6.75 ..."
                      step="0.25"
                      min="0"
                      max="50"
                      className="border-0 px-3 py-3 placeholder-gray-500 text-gray-700 bg-white rounded text-sm shadow focus:outline-none focus:ring w-full"
                      onBlur={(event) => setUnitPrice(event.target.value)}
                    />
                  </div>
                  <div className="relative w-full mb-3">
                    <label
                      className="block uppercase text-gray-700 text-xs font-bold mb-2"
                      htmlFor="category"
                    >
                      Unité <span className="text-red-500">*</span>
                    </label>
                    <select
                      id="category"
                      required
                      defaultValue={unity}
                      className="border-0 px-3 py-3 placeholder-gray-500 text-gray-700 bg-white rounded text-sm shadow focus:outline-none focus:ring w-full"
                      onChange={(event) => setUnity(event.target.value)}
                    >
                      <option value="kg">Kg</option>
                      <option value="pièces">Pièce</option>
                    </select>
                  </div>
                  <div className="relative w-full mb-3">
                    <label
                      className="block uppercase text-gray-700 text-xs font-bold mb-2"
                      htmlFor="stock"
                    >
                      Stock ( unité = unité du produit )&nbsp;<span className="text-red-500">*</span>
                    </label>
                    <input
                      id="stock"
                      type="number"
                      required
                      placeholder="Ex : 3000 (unité du produit)"
                      step="1"
                      min="0"
                      max="10000"
                      className="border-0 px-3 py-3 placeholder-gray-500 text-gray-700 bg-white rounded text-sm shadow focus:outline-none focus:ring w-full"
                      onBlur={(event) => setStock(event.target.value)}
                    />
                  </div>
                </div>
                <div className="relative w-full mb-3">
                  <p className="text-gray-700">
                    (<span className="text-red-500">*</span>) : Champs requis
                  </p>
                </div>

                <div className="text-center mt-6">
                  <button
                    className="bg-green-500 text-white active:bg-green-500 disabled:bg-green-400 text-sm font-bold uppercase px-6 py-3 rounded shadow hover:shadow-lg outline-none focus:outline-none mr-1 mb-1 w-full"
                    type="button"
                    style={{transition: "all .15s ease"}}
                    onClick={() => submitProduct()}
                  >
                    Enregistrer
                  </button>
                </div>
              </form>
            </div>        
          </div>
        )}
        </div>
      </div>
    </>
  );
}
