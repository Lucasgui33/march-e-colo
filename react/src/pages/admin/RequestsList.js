import React, {useContext, useEffect, useState} from "react";

import {AdminContext} from "../../contexts/AdminContext";
import Pagination from "../../components/Pagination";
import RequestCard from "../../components/cards/RequestCard";
import {UserContext} from "../../contexts/UserContext";
import {useHistory} from "react-router-dom";
import usePagination from "../../hooks/usePagination";

export default function RequestsList() {
    const [requests, setRequests] = useState([]);


    const {getRequests} = useContext(AdminContext);
    const {getInfos} = useContext(UserContext);
    const history = useHistory();

    const checkAccess = async () => {
        let infosUser = await getInfos()

        if (infosUser.id !== "invalid") {
            if (!infosUser.roles.includes("ROLE_ADMIN")) {
                history.push("/")
            }
        }
        else {
            history.push("/")
        }
    }

    const fetchData = async () => {
        const r = await getRequests();
        setRequests(r);
    };

    useEffect(() => {
        checkAccess()
        fetchData()
    }, []);

    const {
        next,
        prev,
        jump,
        currentPage,
        maxPage,
        startIndex,
        endIndex,
        paginate,
        jumpIndex
    } = usePagination(requests, 8);

    if (requests === "") {
        return (
            <>
                <div className="fixed top-0 right-0 h-screen w-screen z-50 flex justify-center items-center">
                    <div className="animate-spin rounded-full h-32 w-32 border-t-2 border-b-2 border-gray-900"></div>
                </div>
            </>
        )
    }
    else {
        if (requests.length === 0) {
            return (
                <>
                    <div className="fixed right-0 h-screen w-screen z-50 flex justify-center items-center">
                        <h1>Il n&apos;y a pas de requête à traiter actuellement, veuillez revenir plus tard...</h1>
                    </div>
                </>
            )
        }
        else {
            return (
                <>
                    <div className="grid grid-cols-3 items-center h-full mx-2 ">
                        {requests.slice(startIndex, endIndex).map((request) => {
                            return <RequestCard key={request.id} request={request} />;
                        })}
                    </div>
                    <Pagination
                        currentPage={currentPage}
                        next={next}
                        prev={prev}
                        jump={jump}
                        paginate={paginate}
                        maxPage={maxPage}
                        jumpIndex={jumpIndex}
                    />
                </>
            );
        }
    }
}
