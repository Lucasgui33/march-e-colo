import React, {useContext, useEffect, useState} from "react";
import {useHistory, useParams} from "react-router-dom";

import {AdminContext} from "../../contexts/AdminContext";
import {UserContext} from "../../contexts/UserContext";

export default function RequestDetail() {
    const {getRequest, responseToRequest} = useContext(AdminContext);
    const {getInfos} = useContext(UserContext);

    const params = useParams();
    const history = useHistory();

    const [request, setRequest] = useState({});
    const [message, setMessage] = useState("");

    const checkAccess = async () => {
        let infosUser = await getInfos()

        if (infosUser.id !== "invalid") {
            if (!infosUser.roles.includes("ROLE_ADMIN")) {
                history.push("/")
            }
        }
        else {
            history.push("/")
        }
    }

    const fetchData = async () => {
        const r = await getRequest(params.id);
        setRequest(r);
    };

    useEffect(() => {
        checkAccess()
        fetchData();
    }, []);

    const validate = async () => {

        const update = await responseToRequest({...request, roles: ["ROLE_USER", "ROLE_FARMER"], askProducer: false})

        console.log(update)

        if (update.id === "invalid") {
            setMessage("There seems to have some problems with our servers, sorry for the inconvenience")
        }
        else {
            //envoi de mail

            history.push('/admin/requests');
        }
    };

    const refuse = async () => {



        const update = await responseToRequest({...request, askProducer: false})

        console.log(update)

        if (update.id === "invalid") {
            setMessage("There seems to have some problems with our servers, sorry for the inconvenience")
        }
        else {
            //envoi de mail

            history.push('/admin/requests');
        }
    };

    if (request.length === 0) {
        return (
            <>
                <div className="fixed top-0 right-0 h-screen w-screen z-50 flex justify-center items-center">
                    <div className="animate-spin rounded-full h-32 w-32 border-t-2 border-b-2 border-gray-900"></div>
                </div>
            </>
        )
    }
    else {
        return (
            <>
                <div className="w-full mx-2">
                    {message.length > 0 &&
                        <div className="bg-red-100 border border-red-400 text-red-700 px-4 py-3 rounded relative text-center" role="alert">
                            <span className="block sm:inline">{message}</span>
                        </div>
                    }
                    <br />
                    <div className="bg-white p-3 shadow-sm rounded-sm border-t-4 border-green-400">
                        <div className="flex items-center space-x-2 font-bold text-gray-900 leading-8">
                            <span clas="text-green-500">
                                <svg className="h-5" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"
                                    stroke="currentColor">
                                    <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2"
                                        d="M16 7a4 4 0 11-8 0 4 4 0 018 0zM12 14a7 7 0 00-7 7h14a7 7 0 00-7-7z" />
                                </svg>
                            </span>
                            <span className="tracking-wide">A propos</span>
                        </div>
                        <div className="text-gray-700">
                            <div className="grid md:grid-cols-2 text-sm">
                                <div className="grid grid-cols-2">
                                    <div className="px-4 py-2 font-bold">Nom du producteur</div>
                                    <div className="px-4 py-2">{request.producerName}</div>
                                </div>
                                <div className="grid grid-cols-2">
                                    <div className="px-4 py-2 font-bold">Téléphone</div>
                                    <div className="px-4 py-2">{request.telephone}</div>
                                </div>
                                <div >
                                    <h3 className="px-4 py-2 font-bold">Présentation</h3>
                                    <p className="px-4 py-2">{request.presentation}</p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className="my-4"></div>

                    <div className="bg-white p-3 shadow-sm rounded-sm">

                        <div className="grid grid-cols-2">
                            <button
                                className="border border-green-500 bg-green-500 disabled:bg-green-200 disabled:cursor-not-allowed text-white text-xl font-medium rounded-xl shadow-xl p-3 mx-2 items-center"
                                onClick={() => validate()}
                            >
                                Valider
                            </button>
                            <button
                                className="border border-red-500 bg-red-500 disabled:bg-red-200 disabled:cursor-not-allowed text-white text-xl font-medium rounded-xl shadow-xl p-3 mx-2 items-center"
                                onClick={() => refuse()}
                            >
                                Refuser
                            </button>
                        </div>
                    </div>
                </div>
            </>
        );
    }
}
