import {Link, useHistory} from "react-router-dom";
import React, {useContext, useState} from "react";

import {AuthContext} from "../../contexts/AuthContext";

export default function Login() {

    const {loginDAO, getRoleUser} = useContext(AuthContext);
    const [email, setEmail] = useState("")
    const [password, setPassword] = useState("")
    const [erreur, setErreur] = useState("")
    const history = useHistory()

    if (localStorage.getItem("AuthToken")) {
        localStorage.removeItem("AuthToken");
        localStorage.removeItem("Roles")
    }

    const login = async () => {
        if (email === "" || password === "") {
            setErreur("There is still empty fields to fill")
        }
        else {

            let retourLogin = await loginDAO(email, password);

            if (retourLogin.token === "invalid") {
                setErreur("The email or password you entered is not correct.")
            }
            else if (retourLogin.token === "erreur serveur") {
                setErreur("There seems to have some problems with our servers, sorry for the inconvenience")
            }
            else {
                localStorage.setItem("AuthToken", retourLogin.token)
                const roles = await getRoleUser();
                await localStorage.setItem("Roles", {roles});
                history.push("/")
            }
        }
    }

    return (
        <>
            <main>
                <section className="mt-10 w-full h-full">
                    {erreur.length > 0 &&
                        <div className="bg-red-100 border border-red-400 text-red-700 px-4 py-3 rounded relative text-center" role="alert">
                            <span className="block sm:inline">{erreur}</span>
                        </div>
                    }
                    <br />
                    <div className="mx-auto px-4 h-full">
                        <div className="flex content-center items-center justify-center h-full">
                            <div className="w-full lg:w-4/12 px-4">
                                <div className="relative flex flex-col min-w-0 break-words w-full mb-6 shadow-lg rounded-lg bg-gray-300 border-0">
                                    <div className="rounded-t mb-0 px-6 py-6">
                                        <div className="text-center mb-3">
                                            <h6 className="text-green-500 text-sm font-bold">
                                                Sign in
                                            </h6>
                                        </div>
                                        <form>
                                            <div className="relative w-full mb-3">
                                                <label
                                                    className="block uppercase text-gray-700 text-xs font-bold mb-2"
                                                    htmlFor="grid-password"
                                                >
                                                    Email
                                                </label>
                                                <input
                                                    type="email"
                                                    className="border-0 px-3 py-3 placeholder-gray-400 text-gray-700 bg-white rounded text-sm shadow focus:outline-none focus:ring w-full"
                                                    placeholder="Email"
                                                    style={{transition: "all .15s ease"}}
                                                    onChange={(event) => setEmail(event.target.value)}
                                                />
                                            </div>

                                            <div className="relative w-full mb-3">
                                                <label
                                                    className="block uppercase text-gray-700 text-xs font-bold mb-2"
                                                    htmlFor="grid-password"
                                                >
                                                    Password
                                                </label>
                                                <input
                                                    type="password"
                                                    className="border-0 px-3 py-3 placeholder-gray-400 text-gray-700 bg-white rounded text-sm shadow focus:outline-none focus:ring w-full"
                                                    placeholder="Password"
                                                    style={{transition: "all .15s ease"}}
                                                    onChange={(event) => setPassword(event.target.value)}
                                                />
                                            </div>

                                            <div className="text-center mt-6">
                                                <button
                                                    className="bg-green-500 text-white active:bg-green-500 text-sm font-bold uppercase px-6 py-3 rounded shadow hover:shadow-lg outline-none focus:outline-none mr-1 mb-1 w-full"
                                                    type="button"
                                                    style={{transition: "all .15s ease"}}
                                                    onClick={login}
                                                >
                                                    Sign In
                                                </button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                                <div className="flex flex-wrap mt-6">
                                    <div className="w-1/2">
                                        <Link
                                            to="/forgotPassword"
                                            className="text-green-500"
                                        >
                                            Forgot password ?
                                        </Link>
                                    </div>
                                    <div className="w-1/2 text-right">
                                        <Link
                                            to="/register"
                                            className="text-green-500"
                                        >
                                            Create new account
                                        </Link>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </main>
        </>
    );
}