import {Link, useHistory} from "react-router-dom";
import React, {useContext, useState} from "react";

import {AuthContext} from "../../contexts/AuthContext";

const bcrypt = require("bcryptjs");

export default function Register() {
  const {registerDAO} = useContext(AuthContext);
  const [user, setUser] = useState({
    "firstName": "",
    "lastName": "",
    "adresse": "",
    "email": "",
    "password": ""
  });
  const [password, setPassword] = useState("");
  const [erreur, setErreur] = useState("");
  const history = useHistory();

  const checkPassword = (passwordToCheck) => {
    if (passwordToCheck.length === 0) {
      setPassword("")
      setErreur("")
    }
    else if (passwordToCheck.length < 6) {
      setErreur("Your password must be at leat 6 characters long")
    }
    else {
      setPassword(passwordToCheck)
      setErreur("")
    }
  }

  const checkPasswordConfirmation = (passwordConfirmation) => {
    if (password === passwordConfirmation) {
      setErreur("")
      setUser({...user, password: bcrypt.hashSync(passwordConfirmation, bcrypt.genSaltSync(12))})
    }
    else if (passwordConfirmation.length === 0) {
      setErreur("")
      setUser({...user, password: ""})
    }
    else {
      setErreur("Your confirmation password does not match the password you entered")
    }
  }

  const register = async () => {
    if (
      user.firstName === "" ||
      user.lastName === "" ||
      user.adresse === "" ||
      user.email === "" ||
      user.password === ""
    ) {
      setErreur("There is still empty fields to fill");
    } else {
      let retourRegistration = await registerDAO(
        user
      );

      if (retourRegistration.id !== "invalid") {
        history.push("/login");
      } else {
        setErreur("There is already exist an account with this email.");
      }
    }
  };

  return (
    <>
      <main>
        <section className="mt-10 w-full h-full">
          {erreur.length > 0 && (
            <div
              className="bg-red-100 border border-red-400 text-red-700 px-4 py-3 rounded relative text-center"
              role="alert"
            >
              <span className="block sm:inline">{erreur}</span>
            </div>
          )}
          <br />
          <div className="mx-auto px-4 h-full">
            <div className="flex content-center items-center justify-center h-full">
              <div className="w-full lg:w-4/12 px-4">
                <div className="relative flex flex-col min-w-0 break-words w-full mb-6 shadow-lg rounded-lg bg-gray-300 border-0">
                  <div className="rounded-t mb-0 px-6 py-6">
                    <div className="text-center mb-3">
                      <h6 className="text-green-500 text-sm font-bold">
                        Register in
                      </h6>
                    </div>
                    <form>
                      <div className="relative w-full mb-3">
                        <label
                          className="block uppercase text-gray-700 text-xs font-bold mb-2"
                          htmlFor="grid-password"
                        >
                          First Name
                        </label>
                        <input
                          type="text"
                          className="border-0 px-3 py-3 placeholder-gray-400 text-gray-700 bg-white rounded text-sm shadow focus:outline-none focus:ring w-full"
                          placeholder="First Name"
                          style={{transition: "all .15s ease"}}
                          onChange={(event) => setUser({...user, firstName: event.target.value})}
                        />
                      </div>
                      <div className="relative w-full mb-3">
                        <label
                          className="block uppercase text-gray-700 text-xs font-bold mb-2"
                          htmlFor="grid-password"
                        >
                          Last Name
                        </label>
                        <input
                          type="text"
                          className="border-0 px-3 py-3 placeholder-gray-400 text-gray-700 bg-white rounded text-sm shadow focus:outline-none focus:ring w-full"
                          placeholder="Last Name"
                          style={{transition: "all .15s ease"}}
                          onChange={(event) => setUser({...user, lastName: event.target.value})}
                        />
                      </div>
                      <div className="relative w-full mb-3">
                        <label
                          className="block uppercase text-gray-700 text-xs font-bold mb-2"
                          htmlFor="grid-password"
                        >
                          Address
                        </label>
                        <input
                          type="text"
                          className="border-0 px-3 py-3 placeholder-gray-400 text-gray-700 bg-white rounded text-sm shadow focus:outline-none focus:ring w-full"
                          placeholder="Adresse"
                          style={{transition: "all .15s ease"}}
                          onChange={(event) => setUser({...user, adresse: event.target.value})}
                        />
                      </div>
                      <div className="relative w-full mb-3">
                        <label
                          className="block uppercase text-gray-700 text-xs font-bold mb-2"
                          htmlFor="grid-password"
                        >
                          Email
                        </label>
                        <input
                          type="email"
                          className="border-0 px-3 py-3 placeholder-gray-400 text-gray-700 bg-white rounded text-sm shadow focus:outline-none focus:ring w-full"
                          placeholder="Email"
                          style={{transition: "all .15s ease"}}
                          onChange={(event) => setUser({...user, email: event.target.value})}
                        />
                      </div>

                      <div className="relative w-full mb-3">
                        <label
                          className="block uppercase text-gray-700 text-xs font-bold mb-2"
                          htmlFor="grid-password"
                        >
                          Password
                        </label>
                        <input
                          type="password"
                          className="border-0 px-3 py-3 placeholder-gray-400 text-gray-700 bg-white rounded text-sm shadow focus:outline-none focus:ring w-full"
                          placeholder="Password"
                          style={{transition: "all .15s ease"}}
                          onChange={(event) => checkPassword(event.target.value)}
                        />
                      </div>

                      <div className="relative w-full mb-3">
                        <label
                          className="block uppercase text-gray-700 text-xs font-bold mb-2"
                          htmlFor="grid-password"
                        >
                          Password Confirmation
                        </label>
                        <input
                          type="password"
                          className="border-0 px-3 py-3 placeholder-gray-400 text-gray-700 bg-white rounded text-sm shadow focus:outline-none focus:ring w-full"
                          placeholder="Password"
                          style={{transition: "all .15s ease"}}
                          onChange={(event) => checkPasswordConfirmation(event.target.value)}
                        />
                      </div>

                      <div className="text-center mt-6">
                        <button
                          className="bg-green-500 text-white active:bg-green-500 text-sm font-bold uppercase px-6 py-3 rounded shadow hover:shadow-lg outline-none focus:outline-none mr-1 mb-1 w-full"
                          type="button"
                          onClick={register}
                          style={{transition: "all .15s ease"}}
                        >
                          Register In
                        </button>
                      </div>
                    </form>
                  </div>
                </div>
                <div className="flex flex-wrap mt-6">
                  <div className="w-1/2">
                    <Link to="/login" className="text-green-500">
                      Already have an account ?
                    </Link>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      </main>
    </>
  );
}
