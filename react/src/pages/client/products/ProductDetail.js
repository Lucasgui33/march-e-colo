import { Link, useHistory, useParams } from "react-router-dom";
import { MinusIcon, PlusIcon } from "@heroicons/react/outline";
import React, { useContext, useEffect, useState } from "react";

import { CartContext } from "../../../contexts/CartConext";
import ImageGallery from "react-image-gallery";
import { ProductsContext } from "../../../contexts/ProductsContext";

export default function ProductDetail() {
  const { getProduct, deleteProduct } = useContext(ProductsContext);
  const { addProduct } = useContext(CartContext);

  const params = useParams();
  const history = useHistory();

  const [product, setProduct] = useState({});
  const [isLoading, setIsLoading] = useState(true);
  const [quantityChoice, setQuantityChoice] = useState(0);
  const [roles, setRoles] = useState(
    JSON.parse(localStorage.getItem("Roles")).roles || null
  );

  const maxPieces = 50;
  const maxKg = 5000;

  const fetchData = async () => {
    const p = await getProduct(params.id);
    setProduct(p);
    setIsLoading(false);
  };

  useEffect(() => {
    fetchData();
    setRoles(JSON.parse(localStorage.getItem("Roles")).roles || null);
  }, []);

  const plusQuantity = () => {
    if (product.unity === "kg") {
      if (quantityChoice < maxKg) setQuantityChoice(quantityChoice + 100);
    } else {
      if (quantityChoice < maxPieces) setQuantityChoice(quantityChoice + 1);
    }
  };
  const minusQuantity = () => {
    if (quantityChoice > 0) {
      setQuantityChoice(
        product.unity === "kg" ? quantityChoice - 100 : quantityChoice - 1
      );
    }
  };

  const addToCart = () => {
    addProduct(product, quantityChoice);
    history.push("/client/cart");
  };

  const onDelete = async () => {
    await deleteProduct(product.id);
    history.push("/producer/products");
  }

  return (
    <>
      <div className="w-full h-full mt-5">
        {!isLoading && (
          <div className="grid grid-cols-2 gap-12">
            <div className="flex flex-col items-center">
              <div className="w-3/4">
                <ImageGallery
                  defaultImage={"/images/logo192.png"}
                  items={product.productsImages.map(({ imagePath }) => {
                    return {
                      original: `http://localhost:8000/${imagePath}`,
                      thumbnail: `http://localhost:8000/${imagePath}`
                    };
                  })}
                  showPlayButton={false}
                  showBullets={true}
                />
              </div>
            </div>
            <div>
              <h1 className="text-4xl font-bold mb-8">{product.name}</h1>
              <p className="mb-4">
                <span className="font-bold text-lg">Variété :</span>{" "}
                {product.variety} <br />
                <span className="font-bold text-lg">Origine :</span> FRANCE
              </p>
              <p className="mb-4">
                <span className="font-bold text-lg mb-2">
                  Description du produit :
                </span>
                <br />
                {product.description}
              </p>
              <p className="mb-4">
                <span className="font-bold text-lg">Prix : </span>
                <span className="text-green-500 font-semibold text-lg">
                  {product.unit_price} € / {product.unity}
                </span>
              </p>
              {!roles || !roles.includes("ROLE_FARMER") ? (
                <>
                  <div className="flex items-center">
                    <button
                      className="flex border border-green-500 bg-green-500 text-white rounded-full shadow-md mr-2 items-center focus:outline-none"
                      onClick={() => plusQuantity()}
                    >
                      <PlusIcon className="w-6" />
                    </button>
                    <input
                      className="appearance-none border rounded-lg bg-grey-400 text-center w-20"
                      type="number"
                      value={quantityChoice}
                      min={0}
                      max={product.unity === "kg" ? 5000 : 50}
                      readOnly
                    />
                    <button
                      className="flex border border-red-500 bg-red-500 text-white rounded-full shadow-md w-6 ml-2 items-center focus:outline-none"
                      onClick={() => minusQuantity()}
                    >
                      <MinusIcon className="w-6" />
                    </button>
                    &nbsp;{product.unity === "kg" ? "g" : "pièces"}
                  </div>
                  <div className="flex items-center mt-4">
                    <button
                      className="border border-blue-500 bg-blue-500 disabled:bg-blue-200 disabled:cursor-not-allowed text-white text-xl font-medium rounded-xl shadow-xl p-3 mx-2 items-center"
                      onClick={() => addToCart()}
                      disabled={quantityChoice === 0}
                    >
                      Ajouter au panier
                    </button>
                  </div>
                </>
              ) : (
                <>
                  <p>
                    Stock restant : {product.stock} {product.unity}
                  </p>
                  <div className="flex items-center w-full my-2">
                    <div className="grid grid-cols-2 gap-4 items-center p-2 w-full h-full">
                      <button className="bg-gray-600 rounded-lg shadow-md p-2">
                        <Link to={"/producer/products/" + product.id + "/edit"}>
                          <span className="text-white">Modifier / Mettre à jour le stock</span>
                        </Link>
                      </button>
                      <button
                        className="bg-red-600 rounded-lg shadow-md text-white p-2 h-full"
                        onClick={() => onDelete()}
                      >
                        Supprimer le produit
                      </button>
                    </div>
                  </div>
                </>
              )}
            </div>
          </div>
        )}
      </div>
    </>
  );
}
