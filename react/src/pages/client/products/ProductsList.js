import React, {useContext, useEffect, useState} from "react";

import MultiRangeSlider from "../../../components/multirangeSlider/multiRangeSlider";
import Pagination from "../../../components/Pagination";
import ProductCard from "../../../components/cards/ProductCard";
import {ProductsContext} from "../../../contexts/ProductsContext";
import usePagination from "../../../hooks/usePagination";

export default function ProductsList() {
  const [products, setProducts] = useState([]);
  const [initialProducts, setInitialProducts] = useState([]);
  const [checkFruits, setCheckFruits] = useState(false);
  const [checkVegetables, setCheckVegetables] = useState(false);
  const rangeInit = {min: 0, max: 50};
  const [rangeValue, setRangeValue] = useState({min: 0, max: 50});

  const {getProducts} = useContext(ProductsContext);

  const fetchData = async () => {
    const p = await getProducts();
    setProducts(p);
    setInitialProducts(p);

  };

  useEffect(() => {
    if (
      products.length ||
      checkFruits ||
      checkVegetables ||
      Object.entries(rangeValue).toString() !==
      Object.entries(rangeInit).toString()
    ) {
      setProducts(
        initialProducts
          .filter((product) => {
            if (checkVegetables && checkFruits) return product;
            else if (checkFruits) return product.category.name === "fruits";
            else if (checkVegetables) return product.category.name === "légumes";
            else return product;
          })
          .filter(({unit_price}) => {
            return unit_price >= rangeValue.min && unit_price <= rangeValue.max;
          })
      );
    } else {
      fetchData();
    }
  }, [checkFruits, checkVegetables, rangeValue]);

  const {
    next,
    prev,
    jump,
    currentPage,
    maxPage,
    startIndex,
    endIndex,
    paginate,
    jumpIndex
  } = usePagination(products, 8);

  if (products.length === 0 && products !== "") {
    return (
      <>
        <div className="fixed top-0 right-0 h-screen w-screen z-50 flex justify-center items-center">
          <div className="animate-spin rounded-full h-32 w-32 border-t-2 border-b-2 border-gray-900"></div>
        </div>
      </>
    )
  }
  else if (products === "") {
    return (
      <>
        <div
          className="bg-red-100 border border-red-400 text-red-700 px-4 py-3 rounded relative text-center"
          role="alert"
        >
          <span className="block sm:inline">Il n&apos;y a pas de produits en vente actuellement</span>
        </div>
      </>
    )
  }
  else {
    return (
      <>
        <div className="flex justify-center my-2 p-2 w-full text-xl">
          <span className="p-4">
            <input
              type="checkbox"
              className="form-check-input h-4 w-4 mx-2 rounded-sm border border-green-500 checked:bg-green-600 checked:border-green-600"
              value={checkFruits}
              onChange={() => setCheckFruits(!checkFruits)}
            />
            Fruits
          </span>
          <span className="p-4">
            <input
              type="checkbox"
              className="form-check-input h-4 w-4 mx-2 rounded-sm border border-green-500 checked:bg-green-600 checked:border-green-600"
              value={checkVegetables}
              onChange={() => setCheckVegetables(!checkVegetables)}
            />
            Légumes
          </span>
          <span className="p-4">
            Prix (au kg ou à la pièce)
            <MultiRangeSlider
              min={rangeInit.min}
              max={rangeInit.max}
              step={0.25}
              onChange={({min, max}) => {
                if (min !== rangeValue.min || max !== rangeValue.max)
                  setRangeValue({min, max});
              }}
            />
          </span>
        </div>
        <div className="grid grid-cols-4 gap-10 items-center h-full mx-2 ">
          {products.slice(startIndex, endIndex).map((product) => {
            return <ProductCard key={product.id} product={product} />;
          })}
        </div>
        <Pagination
          currentPage={currentPage}
          next={next}
          prev={prev}
          jump={jump}
          paginate={paginate}
          maxPage={maxPage}
          jumpIndex={jumpIndex}
        />
      </>
    );
  }
}
