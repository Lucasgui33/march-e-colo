import React, {useContext, useEffect, useState} from "react";

import {UserContext} from "../../../contexts/UserContext";
import {useHistory} from "react-router-dom";

const bcrypt = require("bcryptjs");

export default function MyProfileEdit() {
    const {getInfos, updateInfos} = useContext(UserContext)
    const [user, setUser] = useState(null)
    const [password, setPassword] = useState("")
    const [erreur, setErreur] = useState("");

    const history = useHistory();

    const getUserConnected = async () => {

        let infosUser = await getInfos()

        if (infosUser.id !== "invalid") {

            if (infosUser.askProducer === true) {
                setUser({...infosUser, roles: ["ROLE_USER", "ROLE_FARMER"]})
            }
            else {
                setUser(infosUser)
            }
        }
        else {
            history.push("/login")
        }

    }

    useEffect(() => {
        getUserConnected()
    }, []);


    const checkPassword = (passwordToCheck) => {
        if (passwordToCheck.length === 0) {
            setPassword("")
            setErreur("")
        }
        else if (passwordToCheck.length < 6) {
            setErreur("Your password must be at leat 6 characters long")
        }
        else {
            setPassword(passwordToCheck)
            setErreur("")
        }
    }

    const checkPasswordConfirmation = (passwordConfirmation) => {
        if (password === passwordConfirmation && passwordConfirmation.length !== 0) {
            setErreur("")
            setUser({...user, password: bcrypt.hashSync(passwordConfirmation, bcrypt.genSaltSync(12))})
        }
        else if (passwordConfirmation.length === 0) {
            setErreur("")
            setUser({...user, password: ""})
        }
        else {
            setErreur("Your confirmation password does not match the password you entered")
        }
    }

    const edit = async () => {

        await updateInfos(user)

        history.push("/profile")
    }


    const isProducteur = () => {

        if (user.presentation === null) {
            setUser({...user, presentation: ""})
        }

        if (user.producerName === null) {
            setUser({...user, producerName: ""})
        }

        if (user.telephone === null) {
            setUser({...user, telephone: ""})
        }

        if (user.roles.includes("ROLE_FARMER")) {

            return (
                <>
                    <div className="w-full md:w-2/12 md:mx-2">
                        <div className="bg-white p-3 border-t-4 border-green-400">
                            <input
                                type="text"
                                className="border-0 px-3 py-3 placeholder-gray-400 text-gray-700 bg-white rounded text-sm shadow focus:outline-none focus:ring w-full"
                                style={{transition: "all .15s ease"}}
                                value={user.producerName}
                                onChange={(event) => setUser({...user, producerName: event.target.value})}
                                placeholder="Procucer Name"
                            />
                            <br /><br />
                            <textarea
                                className="border-0 px-3 py-3 placeholder-gray-400 text-gray-700 bg-white rounded text-sm shadow focus:outline-none focus:ring w-full"
                                style={{transition: "all .15s ease"}}
                                value={user.presentation}
                                onChange={(event) => setUser({...user, presentation: event.target.value})}
                                placeholder="Presentation"
                            />
                        </div>
                    </div>
                    <div className="w-full md:w-10/12 mx-2">
                        <div className="bg-white p-3 shadow-sm rounded-sm">
                            <div className="flex items-center space-x-2 font-bold text-gray-900 leading-8">
                                <span clas="text-green-500">
                                    <svg className="h-5" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"
                                        stroke="currentColor">
                                        <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2"
                                            d="M16 7a4 4 0 11-8 0 4 4 0 018 0zM12 14a7 7 0 00-7 7h14a7 7 0 00-7-7z" />
                                    </svg>
                                </span>
                                <span className="tracking-wide">About</span>
                            </div>
                            <div className="text-gray-700">
                                <div className="grid md:grid-cols-2 text-sm">
                                    <div className="grid grid-cols-2">
                                        <div className="px-4 py-2 font-bold">First Name</div>
                                        <div className="px-4 py-2">
                                            <input
                                                type="text"
                                                className="border-0 px-3 py-3 placeholder-gray-400 text-gray-700 bg-white rounded text-sm shadow focus:outline-none focus:ring w-full"
                                                style={{transition: "all .15s ease"}}
                                                value={user.firstName}
                                                onChange={(event) => setUser({...user, firstName: event.target.value})}
                                                placeholder="First Name"
                                            />
                                        </div>
                                    </div>
                                    <div className="grid grid-cols-2">
                                        <div className="px-4 py-2 font-bold">Last Name</div>
                                        <div className="px-4 py-2">
                                            <input
                                                type="text"
                                                className="border-0 px-3 py-3 placeholder-gray-400 text-gray-700 bg-white rounded text-sm shadow focus:outline-none focus:ring w-full"
                                                style={{transition: "all .15s ease"}}
                                                value={user.lastName}
                                                onChange={(event) => setUser({...user, lastName: event.target.value})}
                                                placeholder="Last Name"
                                            />
                                        </div>
                                    </div>
                                    <div className="grid grid-cols-2">
                                        <div className="px-4 py-2 font-bold">Address</div>
                                        <div className="px-4 py-2">
                                            <input
                                                type="text"
                                                className="border-0 px-3 py-3 placeholder-gray-400 text-gray-700 bg-white rounded text-sm shadow focus:outline-none focus:ring w-full"
                                                style={{transition: "all .15s ease"}}
                                                value={user.adresse}
                                                onChange={(event) => setUser({...user, adresse: event.target.value})}
                                                placeholder="Adresse"
                                            />
                                        </div>
                                    </div>
                                    <div className="grid grid-cols-2">
                                        <div className="px-4 py-2 font-bold">Email.</div>
                                        <div className="px-4 py-2">
                                            <input
                                                type="email"
                                                className="border-0 px-3 py-3 placeholder-gray-400 text-gray-700 bg-white rounded text-sm shadow focus:outline-none focus:ring w-full"
                                                style={{transition: "all .15s ease"}}
                                                value={user.email}
                                                onChange={(event) => setUser({...user, email: event.target.value})}
                                                placeholder="Email"
                                            />
                                        </div>
                                    </div>
                                    <div className="grid grid-cols-2">
                                        <div className="px-4 py-2 font-bold">N° Telephone</div>
                                        <div className="px-4 py-2">
                                            <input
                                                type="text"
                                                className="border-0 px-3 py-3 placeholder-gray-400 text-gray-700 bg-white rounded text-sm shadow focus:outline-none focus:ring w-full"
                                                style={{transition: "all .15s ease"}}
                                                onChange={(event) => setUser({...user, telephone: event.target.value})}
                                                placeholder="N° Telephone"
                                            />
                                        </div>
                                    </div>
                                    <div className="grid grid-cols-2">
                                        <div className="px-4 py-2 font-bold">Password</div>
                                        <div className="px-4 py-2">
                                            <input
                                                type="password"
                                                className="border-0 px-3 py-3 placeholder-gray-400 text-gray-700 bg-white rounded text-sm shadow focus:outline-none focus:ring w-full"
                                                style={{transition: "all .15s ease"}}
                                                onChange={(event) => checkPassword(event.target.value)}
                                                placeholder="Password"
                                            />
                                        </div>
                                    </div>
                                    <div className="grid grid-cols-2">
                                        <div className="px-4 py-2 font-bold">Password confirmation</div>
                                        <div className="px-4 py-2">
                                            <input
                                                type="password"
                                                className="border-0 px-3 py-3 placeholder-gray-400 text-gray-700 bg-white rounded text-sm shadow focus:outline-none focus:ring w-full"
                                                style={{transition: "all .15s ease"}}
                                                onChange={(event) => checkPasswordConfirmation(event.target.value)}
                                                placeholder="Confirmation password"
                                            />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <button
                                onClick={() => edit()}
                                className="block w-full text-white text-sm font-bold rounded-lg bg-green-500 hover:bg-green-400 focus:outline-none focus:bg-green-600 hover:shadow-xs p-3 my-4">Modify my informations</button>
                        </div>
                    </div>
                </>
            )
        }
        else {
            return (
                <>
                    <div className="w-full md:w-12/12 mx-2">
                        <div className="bg-white p-3 shadow-sm rounded-sm">
                            <div className="flex items-center space-x-2 font-bold text-gray-900 leading-8">
                                <span clas="text-green-500">
                                    <svg className="h-5" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"
                                        stroke="currentColor">
                                        <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2"
                                            d="M16 7a4 4 0 11-8 0 4 4 0 018 0zM12 14a7 7 0 00-7 7h14a7 7 0 00-7-7z" />
                                    </svg>
                                </span>
                                <span className="tracking-wide">About</span>
                            </div>
                            <div className="text-gray-700">
                                <div className="grid md:grid-cols-2 text-sm">
                                    <div className="grid grid-cols-2">
                                        <div className="px-4 py-2 font-bold">First Name</div>
                                        <div className="px-4 py-2">
                                            <input
                                                type="text"
                                                className="border-0 px-3 py-3 placeholder-gray-400 text-gray-700 bg-white rounded text-sm shadow focus:outline-none focus:ring w-full"
                                                style={{transition: "all .15s ease"}}
                                                value={user.firstName}
                                                onChange={(event) => setUser({...user, firstName: event.target.value})}
                                                placeholder="First Name"
                                            />
                                        </div>
                                    </div>
                                    <div className="grid grid-cols-2">
                                        <div className="px-4 py-2 font-bold">Last Name</div>
                                        <div className="px-4 py-2">
                                            <input
                                                type="text"
                                                className="border-0 px-3 py-3 placeholder-gray-400 text-gray-700 bg-white rounded text-sm shadow focus:outline-none focus:ring w-full"
                                                style={{transition: "all .15s ease"}}
                                                value={user.lastName}
                                                onChange={(event) => setUser({...user, lastName: event.target.value})}
                                                placeholder="Last Name"
                                            />
                                        </div>
                                    </div>
                                    <div className="grid grid-cols-2">
                                        <div className="px-4 py-2 font-bold">Address</div>
                                        <div className="px-4 py-2">
                                            <input
                                                type="text"
                                                className="border-0 px-3 py-3 placeholder-gray-400 text-gray-700 bg-white rounded text-sm shadow focus:outline-none focus:ring w-full"
                                                style={{transition: "all .15s ease"}}
                                                value={user.adresse}
                                                onChange={(event) => setUser({...user, adresse: event.target.value})}
                                                placeholder="Adresse"
                                            />
                                        </div>
                                    </div>
                                    <div className="grid grid-cols-2">
                                        <div className="px-4 py-2 font-bold">Email.</div>
                                        <div className="px-4 py-2">
                                            <input
                                                type="email"
                                                className="border-0 px-3 py-3 placeholder-gray-400 text-gray-700 bg-white rounded text-sm shadow focus:outline-none focus:ring w-full"
                                                style={{transition: "all .15s ease"}}
                                                value={user.email}
                                                onChange={(event) => setUser({...user, email: event.target.value})}
                                                placeholder="Email"
                                            />
                                        </div>
                                    </div>
                                    <div className="grid grid-cols-2">
                                        <div className="px-4 py-2 font-bold">Password</div>
                                        <div className="px-4 py-2">
                                            <input
                                                type="password"
                                                className="border-0 px-3 py-3 placeholder-gray-400 text-gray-700 bg-white rounded text-sm shadow focus:outline-none focus:ring w-full"
                                                style={{transition: "all .15s ease"}}
                                                onChange={(event) => checkPassword(event.target.value)}
                                                placeholder="Password"
                                            />
                                        </div>
                                    </div><div className="grid grid-cols-2">
                                        <div className="px-4 py-2 font-bold">Password confirmation</div>
                                        <div className="px-4 py-2">
                                            <input
                                                type="password"
                                                className="border-0 px-3 py-3 placeholder-gray-400 text-gray-700 bg-white rounded text-sm shadow focus:outline-none focus:ring w-full"
                                                style={{transition: "all .15s ease"}}
                                                onChange={(event) => checkPasswordConfirmation(event.target.value)}
                                                placeholder="Confirmation password"
                                            />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <button
                                onClick={() => edit()}
                                className="block w-full text-white text-sm font-bold rounded-lg bg-green-500 hover:bg-green-400 focus:outline-none focus:bg-green-600 hover:shadow-xs p-3 my-4">Modify my informations</button>
                        </div>
                    </div>
                </>
            )
        }
    }

    if (user === null) {
        return (
            <>
                <div className="fixed top-0 right-0 h-screen w-screen z-50 flex justify-center items-center">
                    <div className="animate-spin rounded-full h-32 w-32 border-t-2 border-b-2 border-gray-900"></div>
                </div>
            </>
        )
    }
    else {
        return (
            <>
                <div className="bg-gray-100">
                    {erreur.length > 0 && (
                        <div
                            className="bg-red-100 border border-red-400 text-red-700 px-4 py-3 rounded relative text-center"
                            role="alert"
                        >
                            <span className="block sm:inline">{erreur}</span>
                        </div>
                    )}
                    <div className="container mx-auto my-5 p-5">
                        <div className="md:flex no-wrap md:-mx-2 ">
                            {isProducteur()}
                        </div>
                    </div>
                </div >
            </>
        );
    }
}
