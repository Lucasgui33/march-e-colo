import React, {useContext, useEffect, useState} from "react";

import {UserContext} from "../../../contexts/UserContext";
import {useHistory} from "react-router-dom";

export default function MyProfile() {
    const {getInfos, askToBeProducer} = useContext(UserContext)
    const [user, setUser] = useState(null)
    const [message, setMessage] = useState("")

    const history = useHistory();

    const getUserConnected = async () => {

        let infosUser = await getInfos()

        if (infosUser.id !== "invalid") {
            setUser(infosUser)
            if (infosUser.askProducer === true) {
                setMessage("You will receive an email when an administrator has studied your request to become a producer")
            }
        }
        else {
            history.push("/login")
        }

    }

    const isAskingProducer = async () => {
        await askToBeProducer(user)

        history.push("/profile/edit")
    }

    const hasAskedProducer = () => {

        if (user.askProducer === false) {

            return (
                <>
                    <div className="my-4"></div>

                    <div className="bg-white p-3 shadow-sm rounded-sm">

                        <div className="grid">
                            <div>
                                <button
                                    onClick={isAskingProducer}
                                    className="block w-full text-white text-sm font-bold rounded-lg bg-green-500 hover:bg-green-400 focus:outline-none focus:bg-green-600 hover:shadow-xs p-3 my-4">Passer le compte en mode producteur</button>
                            </div>
                        </div>
                    </div>
                </>
            )
        }
        else {
            return (
                <></>
            )
        }
    }

    useEffect(() => {
        getUserConnected()
    }, []);


    const isProducteur = () => {
        if (user.roles.includes("ROLE_FARMER")) {
            return (
                <>
                    <div className="w-full md:w-3/12 md:mx-2">
                        <div className="bg-white p-3 border-t-4 border-green-400">
                            <h1 className="text-gray-900 font-bold text-xl leading-8 my-1">{user.producerName}</h1>
                            <p className="text-sm text-gray-500 hover:text-gray-600 leading-6">{user.presentation}</p>
                        </div>
                    </div>
                    <div className="w-full md:w-9/12 mx-2">
                        <div className="bg-white p-3 shadow-sm rounded-sm">
                            <div className="flex items-center space-x-2 font-bold text-gray-900 leading-8">
                                <span clas="text-green-500">
                                    <svg className="h-5" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"
                                        stroke="currentColor">
                                        <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2"
                                            d="M16 7a4 4 0 11-8 0 4 4 0 018 0zM12 14a7 7 0 00-7 7h14a7 7 0 00-7-7z" />
                                    </svg>
                                </span>
                                <span className="tracking-wide">About</span>
                            </div>
                            <div className="text-gray-700">
                                <div className="grid md:grid-cols-2 text-sm">
                                    <div className="grid grid-cols-2">
                                        <div className="px-4 py-2 font-bold">First Name</div>
                                        <div className="px-4 py-2">{user.firstName}</div>
                                    </div>
                                    <div className="grid grid-cols-2">
                                        <div className="px-4 py-2 font-bold">Last Name</div>
                                        <div className="px-4 py-2">{user.lastName}</div>
                                    </div>
                                    <div className="grid grid-cols-2">
                                        <div className="px-4 py-2 font-bold">Address</div>
                                        <div className="px-4 py-2">{user.adresse}</div>
                                    </div>
                                    <div className="grid grid-cols-2">
                                        <div className="px-4 py-2 font-bold">Telephone</div>
                                        <div className="px-4 py-2">{user.telephone}</div>
                                    </div>
                                    <div className="grid grid-cols-2">
                                        <div className="px-4 py-2 font-bold">Email.</div>
                                        <div className="px-4 py-2">
                                            <a className="text-blue-800" href={"mailto:" + user.email} >{user.email}</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <button
                                onClick={() => history.push("/profile/edit")}
                                className="block w-full text-white text-sm font-bold rounded-lg bg-green-500 hover:bg-green-400 focus:outline-none focus:bg-green-600 hover:shadow-xs p-3 my-4">Modify my informations</button>
                        </div>

                        <div className="my-4"></div>

                        <div className="bg-white p-3 shadow-sm rounded-sm">

                            <div className="grid grid-cols-2">
                                <div>
                                    <button
                                        onClick={() => history.push("/client/orders")}
                                        className="block w-full text-white text-sm font-bold rounded-lg bg-green-500 hover:bg-green-400 focus:outline-none focus:bg-green-600 hover:shadow-xs p-3 my-4">Mes commandes</button>
                                </div>
                                <div>
                                    <button
                                        onClick={() => history.push("/producer/products")}
                                        className="block w-full text-white text-sm font-bold rounded-lg bg-green-500 hover:bg-green-400 focus:outline-none focus:bg-green-600 hover:shadow-xs p-3 my-4">Mes produits</button>
                                </div>
                            </div>
                        </div>

                        <div className="my-4"></div>

                        <div className="bg-white p-3 shadow-sm rounded-sm">

                            <div className="grid">
                                <div>
                                    <button
                                        onClick={() => history.push("/login")}
                                        className="block w-full text-white text-sm font-bold rounded-lg bg-green-500 hover:bg-green-400 focus:outline-none focus:bg-green-600 hover:shadow-xs p-3 my-4">Se déconnecter</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </>
            )
        }
        else if (user.roles.includes("ROLE_ADMIN")) {
            return (
                <>
                    <div className="w-full md:w-12/12 mx-2">
                        <div className="bg-white p-3 shadow-sm rounded-sm">
                            <div className="flex items-center space-x-2 font-bold text-gray-900 leading-8">
                                <span clas="text-green-500">
                                    <svg className="h-5" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"
                                        stroke="currentColor">
                                        <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2"
                                            d="M16 7a4 4 0 11-8 0 4 4 0 018 0zM12 14a7 7 0 00-7 7h14a7 7 0 00-7-7z" />
                                    </svg>
                                </span>
                                <span className="tracking-wide">About</span>
                            </div>
                            <div className="text-gray-700">
                                <div className="grid md:grid-cols-2 text-sm">
                                    <div className="grid grid-cols-2">
                                        <div className="px-4 py-2 font-bold">First Name</div>
                                        <div className="px-4 py-2">{user.firstName}</div>
                                    </div>
                                    <div className="grid grid-cols-2">
                                        <div className="px-4 py-2 font-bold">Last Name</div>
                                        <div className="px-4 py-2">{user.lastName}</div>
                                    </div>
                                    <div className="grid grid-cols-2">
                                        <div className="px-4 py-2 font-bold">Address</div>
                                        <div className="px-4 py-2">{user.adresse}</div>
                                    </div>
                                    <div className="grid grid-cols-2">
                                        <div className="px-4 py-2 font-bold">Email.</div>
                                        <div className="px-4 py-2">
                                            <a className="text-blue-800" href={"mailto:" + user.email} >{user.email}</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <button
                                onClick={() => history.push("/profile/edit")}
                                className="block w-full text-white text-sm font-bold rounded-lg bg-green-500 hover:bg-green-400 focus:outline-none focus:bg-green-600 hover:shadow-xs p-3 my-4">Modify my informations</button>
                        </div>

                        <div className="my-4"></div>

                        <div className="bg-white p-3 shadow-sm rounded-sm">

                            <div className="grid">
                                <div>
                                    <button
                                        onClick={() => history.push("/login")}
                                        className="block w-full text-white text-sm font-bold rounded-lg bg-green-500 hover:bg-green-400 focus:outline-none focus:bg-green-600 hover:shadow-xs p-3 my-4">Se déconnecter</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </>
            )
        }
        else {
            return (
                <>
                    <div className="w-full md:w-12/12 mx-2">
                        <div className="bg-white p-3 shadow-sm rounded-sm">
                            <div className="flex items-center space-x-2 font-bold text-gray-900 leading-8">
                                <span clas="text-green-500">
                                    <svg className="h-5" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"
                                        stroke="currentColor">
                                        <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2"
                                            d="M16 7a4 4 0 11-8 0 4 4 0 018 0zM12 14a7 7 0 00-7 7h14a7 7 0 00-7-7z" />
                                    </svg>
                                </span>
                                <span className="tracking-wide">About</span>
                            </div>
                            <div className="text-gray-700">
                                <div className="grid md:grid-cols-2 text-sm">
                                    <div className="grid grid-cols-2">
                                        <div className="px-4 py-2 font-bold">First Name</div>
                                        <div className="px-4 py-2">{user.firstName}</div>
                                    </div>
                                    <div className="grid grid-cols-2">
                                        <div className="px-4 py-2 font-bold">Last Name</div>
                                        <div className="px-4 py-2">{user.lastName}</div>
                                    </div>
                                    <div className="grid grid-cols-2">
                                        <div className="px-4 py-2 font-bold">Address</div>
                                        <div className="px-4 py-2">{user.adresse}</div>
                                    </div>
                                    <div className="grid grid-cols-2">
                                        <div className="px-4 py-2 font-bold">Email.</div>
                                        <div className="px-4 py-2">
                                            <a className="text-blue-800" href={"mailto:" + user.email} >{user.email}</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <button
                                onClick={() => history.push("/profile/edit")}
                                className="block w-full text-white text-sm font-bold rounded-lg bg-green-500 hover:bg-green-400 focus:outline-none focus:bg-green-600 hover:shadow-xs p-3 my-4">Modify my informations</button>
                        </div>
                        <div className="my-4"></div>

                        <div className="bg-white p-3 shadow-sm rounded-sm">

                            <div className="grid">
                                <div>
                                    <button
                                        onClick={() => history.push("/client/orders")}
                                        className="block w-full text-white text-sm font-bold rounded-lg bg-green-500 hover:bg-green-400 focus:outline-none focus:bg-green-600 hover:shadow-xs p-3 my-4">Mes commandes</button>
                                </div>
                            </div>
                        </div>

                        {hasAskedProducer()}

                        <div className="my-4"></div>

                        <div className="bg-white p-3 shadow-sm rounded-sm">

                            <div className="grid">
                                <div>
                                    <button
                                        onClick={() => history.push("/login")}
                                        className="block w-full text-white text-sm font-bold rounded-lg bg-green-500 hover:bg-green-400 focus:outline-none focus:bg-green-600 hover:shadow-xs p-3 my-4">Se déconnecter</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </>
            )
        }
    }

    if (user === null) {
        return (
            <>
                <div className="fixed top-0 right-0 h-screen w-screen z-50 flex justify-center items-center">
                    <div className="animate-spin rounded-full h-32 w-32 border-t-2 border-b-2 border-gray-900"></div>
                </div>
            </>
        )
    }
    else {
        return (
            <>
                <div className="bg-gray-100">
                    {message.length > 0 && (
                        <div
                            className="bg-green-100 border border-green-400 text-green-700 px-4 py-3 rounded relative text-center"
                            role="alert"
                        >
                            <span className="block sm:inline">{message}</span>
                        </div>
                    )}
                    <div className="container mx-auto my-5 p-5">
                        <div className="md:flex no-wrap md:-mx-2 ">
                            {isProducteur()}
                        </div>
                    </div>
                </div >
            </>
        );
    }
}
