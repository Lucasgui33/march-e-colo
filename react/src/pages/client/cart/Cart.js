import React, { useContext, useEffect, useState } from "react";

import { CartContext } from "../../../contexts/CartConext";
import FlashMessage from 'react-flash-message'
import { OrderContext } from "../../../contexts/OrderContext";
import { TrashIcon } from "@heroicons/react/outline";
import { useHistory } from "react-router-dom";

export default function Cart() {
  const { clearCart, removeProduct, getCartStorage } = useContext(CartContext);
  const { sendOrder } = useContext(OrderContext);
  const [reload, setReload] = useState(false);
  const [cart, setCart] = useState([]);
  const [showFlash, setShowFlash] = useState(false);
  const [responseFlash, setResponseFlash] = useState('');

  const history = useHistory();

  const fetchData = () => {
    setTimeout(async () => {
      const storageCart = await getCartStorage();
      setCart(storageCart);
      setReload(false);
    }, 100)
  }

  useEffect(() => {
    fetchData();
  }, [reload]);

  const removeCart = async () => {
    await clearCart();
    setReload(true);
  };

  const validateOrder = async () => {
    if (localStorage.getItem('AuthToken')) {
      const response = await sendOrder(cart, totalPrice());
      setShowFlash(true);
      if (response) {
        removeCart();
        setResponseFlash('Votre commande a été effectuée avec succès !')
        setTimeout(() => {
          setShowFlash(false);
        }, 5000)
      }
    } else {
      history.push('/login')
    }

  };

  const removeProductFromCart = async (productId) => {
    await removeProduct(productId);
    setReload(true);
  };

  const totalPrice = () => {
    return cart.reduce((acc, { product, quantity }) => {
      if (product.unity === "kg") {
        acc += product.unit_price * (quantity / 1000);
      } else {
        acc += product.unit_price * quantity;
      }
      return acc;
    }, 0);
  };

  return (
    <>
      {showFlash && (
        <FlashMessage duration={3000} persistOnHover={true}>
          <div className="flex justify-center bg-green-500 items-center p-4">
            <strong className="text-white text-lg font-semibold">{responseFlash}</strong>
          </div>
        </FlashMessage>
      )}
      <div className="flex lg:mx-40 md:mx-40 sm:mx-20 xs:mx-10 justify-center grid grid-cols-1 divide-y-4 divide-green-400">
        <div className="flex justify-between p-2">
          <h1 className="text-3xl font-bold">MON PANIER</h1>
          <div>
            {cart.length > 0 && (
              <>
                <button
                  className="border-red-500 bg-red-500 rounded-xl text-white font-semibold p-2 mx-2"
                  onClick={() => removeCart()}
                >
                  Vider le panier
                </button>
                <button
                  className="border-indigo-500 bg-indigo-500 rounded-xl text-white font-semibold p-2 mx-2"
                  onClick={() => validateOrder()}
                >
                  Valider ma commande
                </button>
              </>
            )}
          </div>
        </div>
        <div>
          {cart.map(({ product, quantity }) => {
            return (
              <>
                <div
                  key={product.id}
                  className="py-2 flex w-full grid grid-cols-5 gap-4"
                >
                  <div className="flex justify-center items-center rounded-xl shadow-md bg-gray-300 h-40 w-40">
                    <img
                      className="rounded-xl object-cover h-full w-full"
                      src={
                        product.productsImages.length
                          ? `http://localhost:8000/${product.productsImages[0].imagePath}`
                          : "/images/logo192.png"
                      }
                      alt={product.name}
                    />
                  </div>
                  <div className="col-span-3 px-5">
                    <h2 className="text-xl font-bold">{product.name}</h2>
                    <p className="text-sm">{product.description}</p>
                    <p>
                      <span className="font-medium">Prix unitaire : </span>{product.unit_price} € / {product.unity === "kg" ? "kg" : "pièces"}
                    </p>
                    <p>
                      <span className="font-medium">Quantité achetée : </span>{quantity} {product.unity === "kg" && "g"}
                    </p>
                    <h3 className="text-lg font-semibold">
                      Sous-Total : &nbsp;
                      {(product.unit_price *
                        (product.unity === "kg" ? quantity / 1000 : quantity)).toFixed(2)}&nbsp;€
                    </h3>
                  </div>
                  <div className="flex items-start justify-end">
                    <button onClick={() => removeProductFromCart(product.id)}>
                      <TrashIcon className="border rounded-lg shadow-md bg-red-500 w-10 text-white" />
                    </button>
                  </div>
                </div>
                {cart.slice(-1)[0].product.id !== product.id && (
                  <div className="h-px mb-2 border border-green-400 bg-green-400"></div>
                )}
              </>
            );
          })}
        </div>
        <div>
          <h1 className="flex justify-end text-2xl font-bold">
            Total : {totalPrice()}&nbsp;€
          </h1>
        </div>
      </div>
    </>
  );
}
