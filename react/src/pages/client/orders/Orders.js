import React, {useContext, useEffect, useState} from "react";
import OrderCard from "../../../components/cards/OrderCard";
import Pagination from "../../../components/Pagination";
import {OrderContext} from "../../../contexts/OrderContext";
import usePagination from "../../../hooks/usePagination";

export default function Orders() {
  const statusList = ["Pending", "Active", "Delivered"];

  const {getClientOrders} = useContext(OrderContext);

  const [orders, setOrders] = useState([]);
  const [initialOrders, setInitialOrders] = useState([]);
  const [isLoading, setIsLoading] = useState(false);
  const [statusSelected, setStatusSelected] = useState("");

  useEffect(() => {
    if (orders.length || statusSelected !== "") {
      setOrders(
        statusSelected !== ""
          ? initialOrders.filter(({status}) => status === statusSelected)
          : initialOrders
      );
    } else {
      setIsLoading(true);
      fetchData();
    }
  }, [statusSelected]);

  const fetchData = async () => {
    const o = await getClientOrders();
    setOrders(o.sort((a, b) => new Date(b.createdAt) - new Date(a.createdAt)));
    setInitialOrders(o.sort((a, b) => new Date(b.createdAt) - new Date(a.createdAt)));
    setIsLoading(false);
  };

  const {
    next,
    prev,
    jump,
    currentPage,
    maxPage,
    startIndex,
    endIndex,
    paginate,
    jumpIndex
  } = usePagination(orders, 6);

  return (
    <>
      {!isLoading ? (
        <>
          <div className="flex lg:mx-20 md:mx-10 sm:mx-10 xs:mx-5 justify-center grid grid-cols-1 divide-y-4 divide-green-400">
            <div className="p-2 flex justify-between">
              <h1 className="text-3xl font-bold">Mes commandes</h1>
              <div className="flex items-center justify-center">
                Statut :  
                <select className="appearance-none p-2 mx-2 bg-gray-300 border border-gray-500 rounded-md" value={statusSelected} onChange={(event) => setStatusSelected(event.target.value)}>
                  <option value={""}>
                    Veuillez sélectionner un statut ...
                  </option>
                  {statusList.map((status) => {
                    return (
                      <option
                        className="p-2"
                        key={status}
                        value={status.toLocaleLowerCase()}
                      >
                        {status}
                      </option>
                    );
                  })}
                </select>
              </div>
            </div>
            <div className="grid lg:grid-cols-3 md:grid-cols-2 sm:grid-cols-2 xs:grid-cols-1 gap-10 items-center h-full mx-2 p-4">
              {orders.slice(startIndex, endIndex).map((order) => {
                return <OrderCard order={order} key={order.id} />;
              })}

              {orders.length === 0 && (
                <h2 className="flex justify-center text-xl">Aucune commande n&apos;a été trouvée !</h2>
              )}
            </div>
          </div>
          <Pagination
            currentPage={currentPage}
            next={next}
            prev={prev}
            jump={jump}
            paginate={paginate}
            maxPage={maxPage}
            jumpIndex={jumpIndex}
          />
        </>
      ) : (
        <h1>Chargement ...</h1>
      )}
    </>
  );
}
