import React, { useContext, useEffect, useState } from 'react';
import { useHistory, useParams } from 'react-router-dom';

import { OrderContext } from '../../../contexts/OrderContext';

export default function OrderDetail() {
  const params = useParams();
  const history = useHistory();

  const { getOrder, deleteOrder } = useContext(OrderContext)

  const [order, setOrder] = useState({});
  const [isLoading, setIsLoading] = useState(true);

  const fetchData = async () => {
    const o = await getOrder(params.id);
    setOrder(o);
    setIsLoading(false);
  }

  useEffect(() => {
    setIsLoading(true);
    fetchData();
  }, []);

  const returnToList = () => {
    history.push('/client/orders');
  }

  const onDelete = async () => {
    await deleteOrder(params.id);
    history.push('/client/orders');
  }

  const totalPrice = () => {
    return order.productsOrders.reduce((acc, { product, quantity }) => {
      if (product.unity === "kg") {
        acc += product.unit_price * (quantity / 1000);
      } else {
        acc += product.unit_price * quantity;
      }
      return acc;
    }, 0);
  };

  return (
    <>
      {!isLoading ? (
        <>
          <div className="flex lg:mx-40 md:mx-40 sm:mx-20 xs:mx-10 justify-center grid grid-cols-1 divide-y-4 divide-green-400">
            <div className="flex justify-between p-2">
              <h1 className="text-3xl font-bold">Commande n° {order.id}</h1>
              <div>
                <button
                  className="border-indigo-500 bg-indigo-500 rounded-xl text-white font-semibold p-2 mx-2"
                  onClick={() => returnToList()}
                >
                  Retour à la liste
                </button>
                <button
                  className="border-red-400 bg-red-400 rounded-xl text-white font-semibold p-2 mx-2"
                  onClick={() => onDelete()}
                >
                  Annuler la commande
                </button>
              </div>
            </div>
            <div>
              {order.productsOrders.map(({ product, quantity }) => {
                return (
                  <>
                    <div
                      key={product.id}
                      className="py-2 flex w-full grid grid-cols-5 gap-4"
                    >
                      <div className="flex justify-center items-center rounded-xl shadow-md bg-gray-300 h-40 w-40">
                        <img
                          className="rounded-xl object-cover h-full w-full"
                          src={
                            product.productsImages.length
                              ? `http://localhost:8000/${product.productsImages[0].imagePath}`
                              : "/images/logo192.png"
                          }
                          alt={product.name}
                        />
                      </div>
                      <div className="col-span-3 px-5">
                        <h2 className="text-xl font-bold">{product.name}</h2>
                        <p className="text-sm">{product.description}</p>
                        <p>
                          <span className="font-medium">Prix unitaire : </span>{Number(product.unit_price)} € / {product.unity === "kg" ? "kg" : "pièces"}
                        </p>
                        <p>
                          <span className="font-medium">Quantité achetée : </span>{Number(quantity)} {product.unity === "kg" && "g"}
                        </p>
                        <h3 className="text-lg font-semibold">
                          Sous-Total : &nbsp;
                          {(Number(product.unit_price) *
                            (product.unity === "kg" ? Number(quantity) / 1000 : Number(quantity))).toFixed(2)}&nbsp;€
                        </h3>
                      </div>
                      <div className="flex items-start justify-end">

                      </div>
                    </div>
                    {order.productsOrders.slice(-1)[0].product.id !== product.id && (
                      <div className="h-px mb-2 border border-green-400 bg-green-400"></div>
                    )}
                  </>
                );
              })}
            </div>
            <div>
              <h1 className="flex justify-end text-2xl font-bold">
                Total : {totalPrice()}&nbsp;€
              </h1>
            </div>
          </div>
        </>
      ) : (
        <h1>Chargement ...</h1>
      )}

    </>
  );
}