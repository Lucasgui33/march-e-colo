import React from 'react';
import {useHistory} from "react-router-dom";

export default function Home() {

    const history = useHistory();

    return (
        <>
            <div className="md:flex space-x-16 mt-20 md:mr-0 mr-10">
                <div className="md:flex items-center pl-16 ">
                    <div className="">
                        <h1 className="lg:text-5xl  font-bold leading-tight text-3xl">Bienvenue sur March-E-colo</h1>
                        <p className="mt-4 text-lg font-normal ">Le marché en ligne qui vous donne la possibilité de préparer vos achats en quelques clics avant de venir sur place</p>
                        <div onClick={() => history.push("/client/products")} className="flex mt-10 w-44 justify-center items-center space-x-3 py-3 px-6 bg-green-600 text-white rounded-lg transition-all duration-400 transform hover:scale-105 cursor-pointer hover:shadow-lg">
                            <button className="text-lg text-md ">Découvrir les produits</button>
                            <span>
                                <svg xmlns="http://www.w3.org/2000/svg" className="h-5 w-5" fill="none" viewBox="0 0 24 24" stroke="currentColor" >
                                    <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M11 19l-7-7 7-7m8 14l-7-7 7-7" />
                                </svg>
                            </span>
                        </div>
                    </div>
                </div>
                <div className="max-w-lg pr-24 md:flex justify-center items-center  hidden">
                    <img className="rounded-lg" src="https://images.unsplash.com/photo-1483058712412-4245e9b90334" alt="" />
                </div>
            </div>
        </>
    )
}