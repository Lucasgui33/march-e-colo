import {
    Route,
    Router,
    Switch
} from 'react-router-dom';

import Cart from '../pages/client/cart/Cart';
import Home from '../pages/Home';
import Login from '../pages/auth/Login';
import MyProfile from '../pages/client/profile/MyProfile';
import MyProfileEdit from '../pages/client/profile/MyProfileEdit';
import Navbar from './Navbar';
import ProductDetail from '../pages/client/products/ProductDetail';
import ProductsList from '../pages/client/products/ProductsList';
import React from 'react';
import Register from '../pages/auth/Register';
import RequestDetail from '../pages/admin/RequestDetails';
import RequestsList from '../pages/admin/RequestsList';
import Orders from '../pages/client/orders/Orders';
import OrderDetail from '../pages/client/orders/OrderDetail';
import MyProducts from '../pages/producer/products/MyProducts';
import AddProduct from '../pages/producer/products/AddProduct';

import {createBrowserHistory} from 'history';

const customHistory = createBrowserHistory();

const Routing = () => {
    return (
        <Router history={customHistory}>
            <Navbar />
            <Switch>
                <Route exact path="/" component={Home} />
                <Route exact path="/login" component={Login} />
                <Route exact path="/register" component={Register} />
                <Route exact path="/profile" component={MyProfile} />
                <Route exact path="/profile/edit" component={MyProfileEdit} />
                <Route exact path="/admin/requests" component={RequestsList} />
                <Route exact path="/admin/requests/:id" component={RequestDetail} />
                <Route exact sensitive path="/client/products" component={ProductsList} />
                <Route exact sensitive path="/client/products/:id" component={ProductDetail} />
                <Route exact path="/client/cart" component={Cart} />
                <Route exact sensitive path="/client/orders" component={Orders} />
                <Route exact sensitive path="/client/orders/:id" component={OrderDetail} />
                <Route exact path="/producer/products" component={MyProducts} />
                <Route exact path="/producer/products/new" component={AddProduct} />
            </Switch>
        </Router>
    )

}

//PROBLEME NAVBAR PAS DYNAMIQUE

export default Routing;

