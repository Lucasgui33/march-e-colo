import React, {useEffect, useState} from "react";

import {Link} from "react-router-dom";
import {ShoppingCartIcon} from "@heroicons/react/solid";
import storageChanged from "storage-changed";

export default function Navbar() {
  const [cart, setCart] = useState(
    JSON.parse(localStorage.getItem("cart")) || []
  );

  const [token, setToken] = useState(localStorage.getItem("AuthToken"));
  const [roles, setRoles] = useState(localStorage.getItem("Roles") ? JSON.parse(localStorage.getItem("Roles")).roles : []);
  const [navbarOpen, setNavbarOpen] = useState(false);

  storageChanged("local", {
    eventName: "localChanged"
  });

  useEffect(() => {
    window.addEventListener("localChanged", () => {
      setCart(JSON.parse(localStorage.getItem("cart")));
      setToken(localStorage.getItem("AuthToken"));
      setRoles(localStorage.getItem("Roles") ? JSON.parse(localStorage.getItem("Roles")).roles : []);
    });
  }, []);

  const isAuthAccessProfile = () => {
    if (token) {
      return (
        <>
          <div>
            <Link
              className="px-3 py-2 flex items-center text-xs uppercase font-bold leading-snug text-white hover:opacity-75"
              to="/profile"
            >
              <span className="ml-2">
                <svg className="h-5" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"
                  stroke="currentColor">

                  <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2"
                    d="M16 7a4 4 0 11-8 0 4 4 0 018 0zM12 14a7 7 0 00-7 7h14a7 7 0 00-7-7z" />
                </svg>
              </span>
              <span>Profile</span>
            </Link>
          </div>
        </>
      )
    }
    else {
      return (
        <>
          <div>
            <Link
              className="px-3 py-2 flex items-center text-xs uppercase font-bold leading-snug text-white hover:opacity-75"
              to="/login"
            >
              <i className="fab fa-twitter text-lg leading-lg text-white opacity-75"></i>
              <span className="ml-2">
                Login
              </span>
            </Link>
          </div>
        </>
      )
    }
  }

  const isFarmer = () => {
    if (roles.includes("ROLE_FARMER")) {
      return (
        <>
          <li className="nav-item">
            <Link
              className="px-3 py-2 flex items-center text-xs uppercase font-bold leading-snug text-white hover:opacity-75"
              to="/producer/products"
            >
              <i className="fab fa-twitter text-lg leading-lg text-white opacity-75"></i>
              <span className="ml-2">Mes produits</span>
            </Link>
          </li>
        </>
      )
    }
  }

  const isAuthMenu = () => {
    if (token) {
      if (roles.includes("ROLE_ADMIN")) {
        return (
          <>
            <li className="nav-item">
              <Link
                className="px-3 py-2 flex items-center text-xs uppercase font-bold leading-snug text-white hover:opacity-75"
                to="/admin/requests"
              >
                <i className="fab fa-twitter text-lg leading-lg text-white opacity-75"></i>
                <span className="ml-2">Demandes passage en producteur</span>
              </Link>
            </li>
          </>
        )
      }
      else {
        return (
          <>
            <li className="nav-item">
              <Link
                className="px-3 py-2 flex items-center text-xs uppercase font-bold leading-snug text-white hover:opacity-75"
                to={
                  roles && roles.includes("ROLE_FARMER")
                    ? "/client/orders"
                    : "/client/orders"
                }
              >
                <i className="fab fa-twitter text-lg leading-lg text-white opacity-75"></i>
                <span className="ml-2">Mes commandes</span>
              </Link>
            </li>
            {isFarmer()}
          </>
        );
      }
    }
  };

  const showCart = () => {
    if(!token || !roles || (token && !roles.includes("ROLE_FARMER") && !roles.includes("ROLE_ADMIN"))) {
      return (
        <li className="nav-item">
          <Link
            className="px-3 py-2 flex items-center font-bold leading-snug text-white hover:opacity-75"
            to="/client/cart"
          >
            <ShoppingCartIcon className="w-5 h-5 py-0" />
            <span className="text-xs">{cart ? cart.length : 0}</span>
          </Link>
        </li>
      )
    }
  }

  return (
    <>
      <nav className="relative flex flex-wrap items-center justify-between px-2 py-3 bg-green-500 mb-3">
        <div className="px-4 mx-auto flex flex-wrap items-center justify-between">
          <div className="w-full relative flex justify-between lg:w-auto lg:static lg:block lg:justify-start">
            <Link
              className="text-sm font-bold leading-relaxed inline-block mr-4 py-2 whitespace-nowrap uppercase text-white"
              to="/"
            >
              March-E-colo
            </Link>
            <button
              className="text-white cursor-pointer text-xl leading-none px-3 py-1 border border-solid border-transparent rounded bg-transparent block lg:hidden outline-none focus:outline-none"
              type="button"
              onClick={() => setNavbarOpen(!navbarOpen)}
            >
              <i className="fas fa-bars"></i>
            </button>
          </div>

          <div className="w-full relative flex justify-between lg:w-auto lg:static lg:block lg:justify-start">
            <div
              className={
                "lg:flex flex-grow items-center" +
                (navbarOpen ? " flex" : " hidden")
              }
              id="example-navbar-danger"
            >
              <ul className="flex flex-col lg:flex-row list-none lg:ml-auto">
                <li className="nav-item">
                  <Link
                    className="px-3 py-2 flex items-center text-xs uppercase font-bold leading-snug text-white hover:opacity-75"
                    to="/client/products"
                  >
                    <i className="fab fa-facebook-square text-lg leading-lg text-white opacity-75"></i>
                    <span className="ml-2">Produits</span>
                  </Link>
                </li>
                {isAuthMenu()}
                {showCart()}
              </ul>
            </div>
          </div>
          {isAuthAccessProfile()}
        </div>
      </nav>
    </>
  );
}
