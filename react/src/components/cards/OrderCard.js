import {DotsHorizontalIcon} from "@heroicons/react/solid";
import React from "react";
import {Link} from "react-router-dom";
import moment from 'moment-timezone';
import 'moment/locale/fr'

export default function OrderCard({order}) {
  const {totalPrice, status, createdAt, productsOrders, id} = order;

  const date = moment.tz(createdAt, 'Europe/Paris').locale('fr').format('Do MMMM YYYY, HH:mm')

  return (
    <>
      <div className="flex flex-col bg-gray-300 border rounded-md items-center h-full shadow-xl p-2">
        <div className="grid grid-cols-4 mb-2">
          <div className="col-span-3 flex flex-col">
            <div className="mb-2">
              <h2 className="text-lg font-semibold">#{id}</h2>
              <small>{date}</small>
              <h3 className="font-medium">{productsOrders.length} produit(s)</h3>
            </div>
          </div>
          <div className="flex flex-col">
            <span className="my-1 flex items-center justify-center px-2 py-2 font-bold leading-none text-indigo-100 bg-purple-600 rounded-full shadow-lg">
              {totalPrice}&nbsp;€
            </span>
            <span
              className={
                (status === "active"
                  ? "bg-blue-300"
                  : status === "pending"
                  ? "bg-orange-300"
                  : "bg-green-300") +
                " my-1 flex items-center justify-center px-2 py-1 font-bold leading-none text-white rounded-full shadow-lg"
              }
            >
              {status}
            </span>
          </div>
        </div>
        <div className="flex flex-col my-2 w-full border-t border-b border-green-400">
          {productsOrders
            .slice(0, 3)
            .map(({product, quantity, price, id}) => {
              return (
                <div key={id} className=" p-1 grid grid-cols-4 items-center">
                  <div className="w-20 rounded-md shadow-md">
                    <img
                      className="rounded-md object-cover h-full w-full"
                      src={
                        "http://localhost:8000/" +
                        product.productsImages[0].imagePath
                      }
                    />
                  </div>
                  <div className="col-span-2 items-center">
                    <span className="font-medium">{product.name}</span>
                    <br />
                    <span>{product.user.producerName}</span>
                  </div>
                  <div className="flex flex-col justify-center items-center">
                    <span>
                      {quantity} {product.unity === "kg" && "g" }
                    </span>
                    <span className="font-semibold">
                      {price}&nbsp;€
                    </span>
                  </div>
                </div>
              );
            })}
          {productsOrders.length > 3 && (
            <div className="flex items-center justify-center">
              <DotsHorizontalIcon className="h-8 text-green-400" />
            </div>
          )}
        </div>
        <div className="flex justify-center items-end w-full h-full">
            
          <div className="flex flex-col mt-2 px-20">
            <button className="bg-blue-600 rounded-lg shadow-md p-2">
              <Link to={"/client/orders/" + id}>
                <span className="text-white text-lg">Voir le détail</span>
              </Link>
            </button>
          </div>
        </div>
      </div>
    </>
  );
}
