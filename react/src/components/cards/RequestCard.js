import {Link} from "react-router-dom";
import React from "react";

export default function RequestCard({request}) {

  return (
    <>
      <div className="bg-white p-3 shadow-sm rounded-sm border-t-4 border-green-400">
        <div className="flex items-center space-x-2 font-bold text-gray-900 leading-8 justify-center">
          <div className="text-xl text-center font-bold">
            {request.producerName}
          </div>
        </div>
        <div className="text-center p-2">
          <button className="bg-blue-600 rounded-lg shadow-md p-2">
            <Link to={'/admin/requests/' + request.id}>
              <span className="text-white">Détails de la requête</span>
            </Link>
          </button>
        </div>
      </div>
    </>
  );
}
