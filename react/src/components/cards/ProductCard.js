import { Link } from "react-router-dom";
import React from "react";

export default function ProductCard({ product, role, onDelete }) {
  const { id, name, user, unit_price, unity, variety, stock, productsImages } = product;

  return (
    <>
      <div className="bg-gray-400 border rounded-md items-center h-full shadow-xl">
        <div className="grid grid-cols-2 gap-2 items-start">
          <div>
            <div className="rounded-xl m-4 shadow-md bg-gray-300 h-32 w-32">
              <img
                className="rounded-xl object-cover h-full w-full"
                src={
                  productsImages.length
                    ? `http://localhost:8000/${productsImages[0].imagePath}`
                    : "/images/logo192.png"
                }
                alt={name}
              />
            </div>
          </div>
          <div>
            <div className="flex flex-col justify-center items-center w-full">
              <div className="text-xl text-center font-bold">{name}</div>
              {role !== "producer" && (<div className="text-indigo-600 font-medium text-lg">{user.producerName}</div>)}
              <div>Origine : FRANCE</div>
              <div>Variété : {variety}</div>
              {role === "producer" && (
                <div>Stock restant : {stock} {unity}</div>
              )}
            </div>
          </div>
        </div>
        <div className="grid grid-cols-2 gap-2 items-center p-2">
          <div className="text-center font-medium">
            {unit_price} € / {unity}
          </div>
          <button className="bg-blue-600 rounded-lg shadow-md p-2">
            <Link to={'/client/products/' + product.id}>
              <span className="text-white">Détails du produit</span>
            </Link>
          </button>
        </div>
        {role === "producer" && (
          <div className="grid grid-cols-2 gap-2 items-center p-2">
            <button className="bg-gray-600 rounded-lg shadow-md p-2">
              <Link to={'/producer/products/' + product.id + '/edit'}>
                <span className="text-white">Modifier</span>
              </Link>
            </button>
            <button className="bg-red-600 rounded-lg shadow-md text-white p-2" onClick={() => onDelete(id)}>
              Supprimer
            </button>
          </div>

        )}
      </div>
    </>
  );
}
