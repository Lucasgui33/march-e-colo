import "./index.css";
import "./tailwind.generated.css";

import {AdminProvider} from "./contexts/AdminContext";
import {AuthProvider} from "./contexts/AuthContext";
import {CartProvider} from "./contexts/CartConext";
import {OrderProvider} from "./contexts/OrderContext";
import {ProductsProvider} from "./contexts/ProductsContext";
import React from "react";
import ReactDOM from "react-dom";
import Routing from "./components/Routing";
import {UserProvider} from "./contexts/UserContext";
import {CategoryProvider} from "./contexts/CategoryContext";

const app = (
  <AuthProvider>
    <UserProvider>
      <AdminProvider>
        <CartProvider>
          <OrderProvider>
            <ProductsProvider>
              <CategoryProvider>
                <Routing />
              </CategoryProvider>
            </ProductsProvider>
          </OrderProvider>
        </CartProvider>
      </AdminProvider>
    </UserProvider>
  </AuthProvider>
);

ReactDOM.render(app, document.getElementById("root"));
