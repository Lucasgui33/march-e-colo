import React, {createContext, useEffect, useState} from "react";

const initialCart = {
  addProduct: () => {},
  removeProduct: () => {},
  getCartStorage: () => {},
  clearCart: () => {}
};

export const CartContext = createContext(initialCart);

export const CartProvider = ({children}) => {
  const [cart, setCart] = useState(
    JSON.parse(localStorage.getItem("cart")) || []
  );

  useEffect(() => {
    localStorage.setItem("cart", JSON.stringify(cart));
  }, [cart]);

  const addProduct = (p, q) => {
    const isAlreadyHere = cart.find(({product}) => p.id === product.id);
    if (isAlreadyHere) {
      setCart(
        cart.map(({product, quantity}) => {
          if (p.id === product.id) {
            quantity += q;
          }
          return {product, quantity};
        })
      );
    } else {
      setCart([...cart, {product: p, quantity: q}]);
    }
  };

  const removeProduct = (productId) => {
    const newCart = cart.filter(({product}) => productId !== product.id);
    setCart(newCart);
  };

  const getCartStorage = () => {
    return JSON.parse(localStorage.getItem('cart'));
  };

  const clearCart = () => {
    setCart([]);
  };

  return (
    <CartContext.Provider
      value={{
        addProduct,
        removeProduct,
        getCartStorage,
        clearCart
      }}
    >
      {children}
    </CartContext.Provider>
  );
};
