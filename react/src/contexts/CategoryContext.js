import React, {createContext} from 'react';

const categoryContext = {
    getCategories: () => {}
}

export const CategoryContext = createContext(categoryContext);

export const CategoryProvider = ({children}) => {
    const getCategories = () => {
        const categories = fetch("http://localhost:8000/api/categories", {
            method: 'GET',
            headers: {
                "Content-Type": "application/json",
                Authorization: `Bearer ${localStorage.getItem('AuthToken')}`
            }
        })
            .then((response) => {
                return response.json();
            })
            .catch((error) => {
                console.error(error)
            });

        return categories
    }

    return (
        <CategoryContext.Provider value={{
            getCategories
        }}>
            {children}
        </CategoryContext.Provider>
    )
}