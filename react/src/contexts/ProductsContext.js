import React, {createContext} from 'react';

const initialProducts = {
    getProducts: () => { },
    getProducerProducts: () => { },
    getProduct: () => { },
    deleteProduct: () => { },
    addProduct: () => { }
}

export const ProductsContext = createContext(initialProducts);

export const ProductsProvider = ({children}) => {
    const getProducts = () => {
        const products = fetch(`http://localhost:8000/api/products`, {
            method: 'GET',
            headers: {
                "Content-Type": "application/json"
            }
        })
            .then((response) => {
                return response.json()
            })
            .catch((error) => {
                console.error(error)
                return ""
            });
        return products;
    }

    const getProduct = (id) => {
        const products = fetch(`http://localhost:8000/api/products/${id}`, {
            method: 'GET',
            headers: {
                "Content-Type": "application/json"
            }
        })
            .then((response) => {
                return response.json()
            })
            .catch((error) => {
                console.error(error)
            });
        return products;
    }

    const getProducerProducts = () => {
        const products = fetch(`http://localhost:8000/api/myproducts`, {
            method: "GET",
            headers: {
                "Content-Type": "application/json",
                Authorization: `Bearer ${localStorage.getItem('AuthToken')}`
            }
        })
            .then((response) => {
                return response.json();
            })
            .catch((error) => {
                console.error(error);
            });
        return products;
    }

    const deleteProduct = (productId) => {
        fetch(`http://localhost:8000/api/products/${productId}`, {
            method: "DELETE",
            headers: {
                "Content-Type": "application/json",
                Authorization: `Bearer ${localStorage.getItem('AuthToken')}`
            }
        })
            .catch((error) => {
                console.error(error);
            });
    }

    const addProduct = (name, description, variety, category, unitPrice, unity, stock, productsImages) => {
        const formData = new FormData();
        formData.append("name", name);
        formData.append("description", description);
        formData.append("variety", variety);
        formData.append("category", category);
        formData.append("unit_price", unitPrice);
        formData.append("unity", unity);
        formData.append("stock", stock);
        for (let img of productsImages[0]) {
            formData.append('images[]', img, "image.png");
        }

        return fetch(`http://localhost:8000/api/producte`, {
            method: 'POST',
            headers: {
                Authorization: `Bearer ${localStorage.getItem('AuthToken')}`
            },
            body: formData
        }).then((response) => {
            return response.json();
        })
            .catch((error) => {
                console.error(error);
            });
    }

    return (
        <ProductsContext.Provider value={{
            getProducts,
            getProducerProducts,
            getProduct,
            deleteProduct,
            addProduct
        }}>
            {children}
        </ProductsContext.Provider>
    )
}