import React, {createContext} from 'react';

const authContext = {
    loginDAO: () => { },
    registerDAO: () => { },
    getRoleUser: () => { }
}

export const AuthContext = createContext(authContext);

export const AuthProvider = ({children}) => {
    const loginDAO = (email, password) => {
        const user = fetch("http://localhost:8000/api/login_check", {
            method: 'POST',
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify({
                "username": email,
                password
            })
        })
            .then((response) => {
                if (!response.ok) {
                    return {token: "invalid"}
                }
                else {
                    return response.json()
                }
            })
            .then((responseJSON) => {
                return responseJSON
            })
            .catch((error) => {
                console.error(error)
                return {token: "erreur serveur"}
            });

        return user
    }

    const registerDAO = (user) => {

        const isRegistered = fetch("http://localhost:8000/api/users", {
            method: 'POST',
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify({
                "email": user.email,
                "roles": ["ROLE_USER"],
                "password": user.password,
                "firstName": user.firstName,
                "lastName": user.lastName,
                "adresse": user.adresse
            })
        })
            .then((response) => {
                if (!response.ok) {
                    return {id: "invalid"}
                }
                return response.json()
            })
            .then((responseJSON) => {
                return responseJSON
            })
            .catch((error) => {
                console.error("error", error)
                return error
            });

        return isRegistered
    }

    const getRoleUser = () => {
        const roles = fetch(`http://localhost:8000/api/myroles`, {
            method: "GET",
            headers: {
                "Content-Type": "application/json",
                Authorization: `Bearer ${localStorage.getItem('AuthToken')}`
            }
        })
            .then((response) => {
                return response.json();
            })
            .catch((error) => {
                console.error(error);
            });

        return roles;
    }

    return (
        <AuthContext.Provider value={{
            loginDAO,
            registerDAO,
            getRoleUser
        }}>
            {children}
        </AuthContext.Provider>
    )
}