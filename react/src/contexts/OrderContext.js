import React, {createContext} from "react";

const initialOrder = {
  getClientOrders: () => { },
  getOrder: () => { },
  deleteOrder: () => { },
  sendOrder: () => { }
};

export const OrderContext = createContext(initialOrder);

export const OrderProvider = ({children}) => {
  function setNewOrder(cart, totalPrice) {
    return {
      totalPrice: totalPrice.toString(),
      status: "pending",
      productsOrders: cart.map(({product, quantity}) => {
        const {unit_price, unity} = product;
        return {
          product: `/api/products/${product.id}`,
          quantity,
          price: (unity === "kg"
            ? (unit_price * quantity) / 1000
            : unit_price * quantity
          ).toString()
        };
      })
    };
  }

  const sendOrder = (cart, totalPrice) => {
    const newOrder = setNewOrder(cart, totalPrice);

    const postOrder = fetch(`http://localhost:8000/api/orders`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem('AuthToken')}`
      },
      body: JSON.stringify(newOrder)
    })
      .then((response) => {
        return response.json();
      })
      .catch((error) => {
        console.error(error);
      });
    return postOrder;
  };

  const getClientOrders = () => {
    const orders = fetch(`http://localhost:8000/api/myorders`, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem('AuthToken')}`
      }
    })
      .then((response) => {
        return response.json();
      })
      .catch((error) => {
        console.error(error);
      });
    return orders;
  };

  const getOrder = (idOrder) => {
    const order = fetch(`http://localhost:8000/api/orders/${idOrder}`, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem('AuthToken')}`
      }
    })
      .then((response) => {
        return response.json();
      })
      .catch((error) => {
        console.error(error);
      });
    return order;
  };

  const deleteOrder = (idOrder) => {
    const order = fetch(`http://localhost:8000/api/orders/${idOrder}`, {
      method: "DELETE",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem('AuthToken')}`
      }
    })
      .then((response) => {
        return response.json();
      })
      .catch((error) => {
        console.error(error);
      });
    return order;
  };

  return (
    <OrderContext.Provider
      value={{
        getClientOrders,
        getOrder,
        sendOrder,
        deleteOrder
      }}
    >
      {children}
    </OrderContext.Provider>
  );
};
