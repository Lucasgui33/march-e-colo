import React, {createContext} from 'react';

const adminContext = {
    getRequests: () => { },
    getRequest: () => { },
    responseToRequest: () => { }
}

export const AdminContext = createContext(adminContext);

export const AdminProvider = ({children}) => {

    const getRequests = () => {

        const isRegistered = fetch("http://localhost:8000/api/getrequests", {
            method: 'GET',
            headers: {
                "Content-Type": "application/json",
                Authorization: `Bearer ${localStorage.getItem('AuthToken')}`
            }
        })
            .then((response) => {
                if (!response.ok) {
                    return []
                }
                return response.json()
            })
            .then((responseJSON) => {
                return responseJSON
            })
            .catch((error) => {
                console.error("error", error)
                return error
            });

        return isRegistered
    }

    const getRequest = (id) => {
        const request = fetch(`http://localhost:8000/api/users/${id}`, {
            method: 'GET',
            headers: {
                "Content-Type": "application/json"
            }
        })
            .then((response) => {
                if (!response.ok) {
                    return {id: "invalid"}
                }
                else {
                    return response.json()
                }
            })
            .then((responseJSON) => {
                return responseJSON
            })
            .catch((error) => {
                console.error("error", error)
                return error
            });
        return request;
    }
    const responseToRequest = (user) => {
        const isUpdated = fetch("http://localhost:8000/api/users/" + user.id, {
            method: 'PATCH',
            headers: {
                "Content-Type": "application/json",
                Authorization: `Bearer ${localStorage.getItem('AuthToken')}`
            },
            body: JSON.stringify({
                "roles": user.roles,
                "askProducer": user.askProducer
            })
        })
            .then((response) => {
                if (!response.ok) {
                    return {id: "invalid"}
                }
                else {
                    return response.json()
                }
            })
            .then((responseJSON) => {
                return responseJSON
            })
            .catch((error) => {
                console.error(error)
            });

        return isUpdated
    }

    return (
        <AdminContext.Provider value={{
            getRequests,
            getRequest,
            responseToRequest
        }}>
            {children}
        </AdminContext.Provider>
    )
}