import React, {createContext} from 'react';

const userContext = {
    getInfos: () => { },
    updateInfos: () => { },
    askToBeProducer: () => { }
}

export const UserContext = createContext(userContext);

export const UserProvider = ({children}) => {
    const getInfos = () => {
        const user = fetch("http://localhost:8000/api/myprofile", {
            method: 'GET',
            headers: {
                "Content-Type": "application/json",
                Authorization: `Bearer ${localStorage.getItem('AuthToken')}`
            }
        })
            .then((response) => {
                if (!response.ok) {
                    return {id: "invalid"}
                }
                else {
                    return response.json()
                }
            })
            .then((responseJSON) => {
                return responseJSON
            })
            .catch((error) => {
                console.error(error)
                return {id: "invalid"}
            });

        return user
    }
    const updateInfos = (user) => {
        if (user.password === "") {
            //modif d'api à faire, route custom pour verif si id utilisateur == utilisateur token
            const isUpdated = fetch("http://localhost:8000/api/users/" + user.id, {
                method: 'PATCH',
                headers: {
                    "Content-Type": "application/json",
                    Authorization: `Bearer ${localStorage.getItem('AuthToken')}`
                },
                body: JSON.stringify({
                    "email": user.email,
                    "firstName": user.firstName,
                    "lastName": user.lastName,
                    "telephone": user.telephone,
                    "adresse": user.adresse,
                    "presentation": user.presentation,
                    "producerName": user.producerName
                })
            })
                .then((response) => {
                    if (!response.ok) {
                        return {id: "invalid"}
                    }
                    else {
                        return response.json()
                    }
                })
                .then((responseJSON) => {
                    return responseJSON
                })
                .catch((error) => {
                    console.error(error)
                });

            return isUpdated
        }
        else {
            const isUpdated = fetch("http://localhost:8000/api/users/" + user.id, {
                method: 'PATCH',
                headers: {
                    "Content-Type": "application/json",
                    Authorization: `Bearer ${localStorage.getItem('AuthToken')}`
                },
                body: JSON.stringify({
                    "email": user.email,
                    "password": user.password,
                    "firstName": user.firstName,
                    "lastName": user.lastName,
                    "telephone": user.telephone,
                    "adresse": user.adresse,
                    "presentation": user.presentation,
                    "producerName": user.producerName
                })
            })
                .then((response) => {
                    if (!response.ok) {
                        return {id: "invalid"}
                    }
                    else {
                        return response.json()
                    }
                })
                .then((responseJSON) => {
                    return responseJSON
                })
                .catch((error) => {
                    console.error(error)
                });

            return isUpdated
        }
    }
    const askToBeProducer = (user) => {
        const isUpdated = fetch("http://localhost:8000/api/users/" + user.id, {
            method: 'PATCH',
            headers: {
                "Content-Type": "application/json",
                Authorization: `Bearer ${localStorage.getItem('AuthToken')}`
            },
            body: JSON.stringify({
                "askProducer": true
            })
        })
            .then((response) => {
                return response.json()
            })
            .then((responseJSON) => {
                return responseJSON
            })
            .catch((error) => {
                console.error(error)
            });

        return isUpdated
    }

    return (
        <UserContext.Provider value={{
            getInfos,
            updateInfos,
            askToBeProducer
        }}>
            {children}
        </UserContext.Provider>
    )
}