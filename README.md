# Projet March-E-colo

Ce projet est réalisé par Lucas Guillotin, Yoann Singer, Alexandre Mazeau et Nicolas Vanexem.

March-E-colo met en relation ses adhérents avec des producteurs locaux qu'il a sélectionnés et facilite la vente de produits issus de l'agriculture biologique de proximité.

Les visiteurs peuvent voir les différents produits vendus par les producteurs mais ne peuvent faire une commande qui s'ils devienent adhérent en se créant un compte.

Les clients souhaitant proposer leurs produits doivent déposer une demande auprès de l'administration de March-E-colo.
Celle-ci vérifie si le producteur respecte un ensemble de critères et accepte ou non la demande.



## Installation

1. Clonez ce projet dans un dossier

2. Soyez sur d'avoir une base de données accessible et si vous n'en avez pas, vous pouvez installer WAMP en cliquant <a href="https://www.wampserver.com/en/download-wampserver-64bits/" target="_blank">ici</a>

3. Installez et configurez votre partie Back : <a href="https://gitlab.com/Lucasgui33/march-e-colo/-/tree/master/symfony" target="_blank">Documentation Back</a>

4. Installez et configurez votre partie Front : <a href="https://gitlab.com/Lucasgui33/march-e-colo/-/tree/master/react" target="_blank">Documentation Front</a>
